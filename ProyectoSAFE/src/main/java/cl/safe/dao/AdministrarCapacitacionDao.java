package cl.safe.dao;

import cl.safe.to.AsistenciaCapacitacion;
import cl.safe.to.Capacitacion;
import cl.safe.to.Expositor;
import cl.safe.to.Trabajador;
import java.util.List;

public interface AdministrarCapacitacionDao {
    
    public List<Capacitacion> obtenerCapacitacionPorRutEmpresa(int rutEmpresa);
    public List<Trabajador> obtenerTrabajadorPorEmpresa(int id);
    public  Boolean modificarCapacitacion(Capacitacion cap);
    public  Boolean eliminarAsistenciaMedica(int idAsistenciaCapacitacion);
    public  List<Expositor> obtenerExpositores();
    public  List<AsistenciaCapacitacion> obtenerAsistenciaCapacitacionPorId(int idCapacitacion);
    public  Boolean agregarCapacitacion(Capacitacion cap);
    public  Boolean agregarAsistenciaCapacitacion(AsistenciaCapacitacion cap);
   
    
    
}
