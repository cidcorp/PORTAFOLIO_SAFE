package cl.safe.dao;

import cl.safe.to.EmpresaComuna;
import cl.safe.to.Evaluacion;
import cl.safe.to.EvaluacionRevision;
import cl.safe.to.InstalacionComuna;
import cl.safe.to.PersonaIngeniero;
import cl.safe.to.PersonaTecnico;
import cl.safe.to.RevisionEvaluacionEmpresa;
import cl.safe.to.Tecnico;
import cl.safe.to.Trabajador;
import java.util.List;

public interface AdministrarEvaluacionesDao {
    
    public List<Evaluacion> obtenerEvaluacionPorRut(int rut);
    public Tecnico obtenerTecnico(int id);
    public Trabajador obtenerTrabajadorPorIdTencnico(int id);
    public  List<PersonaTecnico> listarTecnicos();
    public  List<PersonaIngeniero> listarIngenieros();
    public  List<InstalacionComuna> listarInstalacionEmpresas(int rutEmpresa);
    public  List<EmpresaComuna> listarEmpresasComuna();
    public  Boolean agregarEvaluacionInstalacion(Evaluacion eva);
    public  Boolean modificarEvaluacionInstalacion(Evaluacion eva);
    public  Boolean agregarEvaluacionTrabajador(Evaluacion eva);
    public  Boolean modificarEvaluacionTrabajador(Evaluacion eva);
    public  Boolean agregarEvaluacionRevInstalacion(EvaluacionRevision eva);
    public  Boolean modificarEvaluacionRevInstalacion(EvaluacionRevision eva);
    public  Boolean agregarEvaluacionRevTrabajador(EvaluacionRevision eva);
    public  Boolean modificarEvaluacionRevTrabajador(EvaluacionRevision eva);
    public List<RevisionEvaluacionEmpresa> obtenerRevisionEvaluacionesPorIdEmpresa(int idEvaluacion);
    
}
