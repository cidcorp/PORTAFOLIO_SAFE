package cl.safe.dao;

import cl.safe.to.AgendaMedica;
import cl.safe.to.AtencionMedica;
import cl.safe.to.ExamenEmpresa;
import cl.safe.to.ExamenFichaMedica;
import cl.safe.to.FichaMedica;
import cl.safe.to.Medico;
import java.util.List;

public interface AdministrarVisitaMedicaDao {
    
    public List<AgendaMedica> obtenerAgendaMedicaPorRutEmpresa(int rutEmpresa);
    public List<ExamenEmpresa> obtenerExamenesEmpresa(int rutEmpresa);
    public List<FichaMedica> obtenerFichaMedica(int rutEmpresa);
    public List<Medico> obtenerMedicos();
    public AgendaMedica obtenerAgendaMedicaPorId(int idAgendaMedica);
    public Boolean agregarAgendaMedica(AgendaMedica agenda);
    public Boolean modificarAgendaMedica(AgendaMedica agenda);
    public Boolean agregarAtencionMedica(AtencionMedica atencionMedica);
    public Boolean modificarAtencionMedica(AtencionMedica atencionMedica);
    public Boolean agregarFichaMedica(FichaMedica fichaMedica);
    public Boolean modificarFichaMedica(FichaMedica fichaMedica);
    public Boolean agregarExamenFichaMedica(ExamenFichaMedica examenfichaMedica);
    public Boolean eliminarExamenFichaMedica(int id);
    public List<AtencionMedica> obtenerAtencionMedicaPorIdAgendaMedica(int idEmpresa);
    
}
