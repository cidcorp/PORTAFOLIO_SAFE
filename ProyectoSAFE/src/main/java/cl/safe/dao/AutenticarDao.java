package cl.safe.dao;

import cl.safe.to.Login;


public interface AutenticarDao {

    public Login autenticarUsuario(Login login);
    
}
