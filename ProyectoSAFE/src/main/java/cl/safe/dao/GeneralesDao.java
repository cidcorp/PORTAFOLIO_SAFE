package cl.safe.dao;

import cl.safe.to.Comuna;
import cl.safe.to.Region;
import java.util.List;

public interface GeneralesDao {
    
    public List<Region> obtenerRegiones();    
    public List<Comuna> obtenerComunaPorRegion(int idRegion);
    public Boolean onBd();
}
