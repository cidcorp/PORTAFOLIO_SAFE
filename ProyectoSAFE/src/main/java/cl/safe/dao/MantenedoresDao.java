package cl.safe.dao;

import cl.safe.to.Administrador;
import cl.safe.to.AsociacionPerfilMenu;
import cl.safe.to.Empresa;
import cl.safe.to.Instalacion;
import cl.safe.to.Medico;
import cl.safe.to.MenuTo;
import cl.safe.to.Perfil;
import cl.safe.to.Persona;
import cl.safe.to.Supervisor;
import cl.safe.to.Trabajador;
import cl.safe.to.Usuario;
import cl.safe.to.UsuarioAuth;
import java.util.List;

public interface MantenedoresDao {
    public Boolean agregarEmpresa(Empresa emp);
    public Boolean modificarEmpresa(Empresa emp);
    public Boolean agregarUsuario(Usuario usu);
    public Boolean modificarUsuario(Usuario usu);
    public Boolean agregarPersona(Persona persona);
    public Boolean modificarPersona(Persona persona);
    public Boolean agregarMedico(Medico medico);
    public Boolean modificarMedico(Medico medico);
    public Boolean agregarTrabajador(Trabajador trabajador);
    public Boolean modificarTrabajador(Trabajador trabajador);
    public Boolean agregarInstalacion(Instalacion instalacion);
    public Boolean modificarInstalacion(Instalacion instalacion);
    public List<Supervisor> obtenerSupervisores();
    public List<Perfil> obtenerPerfiles();
    public List<MenuTo> obtenerMenu();
    public List<UsuarioAuth> obtenerUsuario();
    public Persona obtenerPersona(int rutPersona);
    public List<Administrador> obtenerAdministrador();   
    public List<AsociacionPerfilMenu> obtenerAsociacion_perfil_menu();
}
