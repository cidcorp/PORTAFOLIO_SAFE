package cl.safe.dao;

import cl.safe.to.Persona;

/**
 *
 * @author CidCorp
 */
public interface PersonaDao {
    
    public Persona obtenerPersonaPorRut(int id);
    
}
