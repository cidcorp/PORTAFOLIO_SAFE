
package cl.safe.dao.impl;

import cl.safe.dao.AdministrarCapacitacionDao;
import cl.safe.rowMapper.AsistenciaCapacitacionMapper;
import cl.safe.rowMapper.CapacitacionMapper;
import cl.safe.rowMapper.ExpositorPersonaMapper;
import cl.safe.rowMapper.TrabajadorPersonaMapper;
import cl.safe.to.AsistenciaCapacitacion;
import cl.safe.to.Capacitacion;
import cl.safe.to.Expositor;
import cl.safe.to.Trabajador;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class AdministrarCapacitacionDaoImpl implements AdministrarCapacitacionDao {
    
    private JdbcTemplate jdbctemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate; 
    
    @Autowired
    @Qualifier("dataSource")
    public void setDatasource(DataSource dataSource){
        jdbctemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    public List<Capacitacion> obtenerCapacitacionPorRutEmpresa(int rutEmpresa) {
        
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT ID_CAPACITACION,ASUNTO_CAPACITACION,FECHA_INICIO,FECHA_TERMINO, ");
        sql.append("CUPO_MINIMO,ID_SUPERVISOR,ID_EXPOSITOR,ID_EMPRESA,CANTIDAD_ASISTENTES ");
        sql.append("FROM capacitaciones ");
        sql.append("WHERE ID_EMPRESA = ? ");
        
        return jdbctemplate.query(sql.toString(),new Object[]{rutEmpresa}, new CapacitacionMapper()); 
    }
            
    public List<Trabajador> obtenerTrabajadorPorEmpresa(int idEmpresa) {
        
        Trabajador trabajador = new Trabajador();
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT p.RUT_PERSONA,p.NOMBRE_PERSONA,p.APEPAT_PERSONA,p.APEMAT_PERSONA, ");
        sql.append("p.EMAIL_PERSONA,p.DIRECCION_PERSONA,p.FECHA_INGRESO,p.CELULAR_PERSONA, ");
        sql.append("p.FECHA_NACIMIENTO,p.ID_COMUNA,p.ID_EMPRESA,p.ESTADO, t.id_trabajador,t.cargo_trabajador ");
        sql.append("FROM PERSONA p ");
        sql.append("INNER JOIN TRABAJADOR t ");
        sql.append("ON p.rut_persona = t.rut_persona ");
        sql.append("WHERE p.ID_EMPRESA = ?");

        return jdbctemplate.query(sql.toString(),new Object[]{idEmpresa}, new TrabajadorPersonaMapper());
    }

    public Boolean modificarCapacitacion(Capacitacion cap) {
        Boolean resultado=true;
        try {
             jdbctemplate.update(
                "{ call Insertempresa(?,?,?,?,?,?,?,?,?) }",
                cap.getId(),cap.getAsunto(),cap.getFechaInicio(),
                cap.getFechaTermino(), cap.getCupoMinimo(), cap.getIdSupervisor(),
                cap.getIdExpositor(), cap.getIdEmpresa(),cap.getCantidadAsistencia()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado = false; 
        }
        return resultado;   
    }
    
    public Boolean eliminarAsistenciaMedica(int idAsistenciaCapacitacion) {
        return true;
//        StringBuilder sql = new StringBuilder(100);
//        sql.append(" ");
//        sql.append(" ");
//        
//        return jdbctemplate.query(sql.toString(),new Object[]{rutEmpresa}, new CapacitacionMapper()); 
    }
    
    public List<Expositor> obtenerExpositores() {
        
        StringBuilder sql = new StringBuilder(100);
        sql.append("select p.RUT_PERSONA,p.NOMBRE_PERSONA,p.APEPAT_PERSONA,p.APEMAT_PERSONA ");
        sql.append(",p.EMAIL_PERSONA,p.DIRECCION_PERSONA,p.FECHA_INGRESO,p.CELULAR_PERSONA ");
        sql.append(",p.FECHA_NACIMIENTO,p.ID_COMUNA,p.ID_EMPRESA,p.ESTADO,ex.ID_EXPOSITOR ");
        sql.append("from expositor ex ");
        sql.append("inner join persona p ");
        sql.append("on p.rut_persona = ex.rut_persona ");
        
        return jdbctemplate.query(sql.toString(), new ExpositorPersonaMapper()); 
    }
    
    public List<AsistenciaCapacitacion> obtenerAsistenciaCapacitacionPorId(int idCapacitacion) {
        
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT ID_CAPACITACION,FECHA,REGISTRO_ASISTENCIA,ID_TRABAJADOR  "); 
        sql.append("FROM ASISTENCIA_CAPACITACION "); 
        sql.append("WHERE ID_CAPACITACION = ? "); 
        
        return jdbctemplate.query(sql.toString(),new Object[]{idCapacitacion}, new AsistenciaCapacitacionMapper()); 
    }
    
    public Boolean agregarCapacitacion(Capacitacion cap) {
        Boolean resultado=true;
        try {
             jdbctemplate.update(
                "{ call SP_INSERT_CAPACITACIONES(?,?,?,?,?,?,?,?,?) }",
                cap.getId(),cap.getAsunto(),cap.getFechaInicio(),
                cap.getFechaTermino(), cap.getCupoMinimo(), cap.getIdSupervisor(),
                cap.getIdExpositor(), cap.getIdEmpresa(),cap.getCantidadAsistencia()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado = false;
        }
        return resultado;   
    }
    
    public Boolean agregarAsistenciaCapacitacion(AsistenciaCapacitacion asistencia) {
        Boolean resultado=true;
        System.out.println("asistencia " + asistencia.toString());
        try {
             jdbctemplate.update(
                "{ call SP_INSERT_AC(?,?,?,?,?)",
                asistencia.getId(),asistencia.getFecha(),asistencia.getRegistro(),
                asistencia.getIdTrabajador(),asistencia.getId()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado = false;
        }
        return resultado;   
    }
    
}
