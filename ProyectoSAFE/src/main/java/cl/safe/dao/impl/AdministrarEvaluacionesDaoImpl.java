
package cl.safe.dao.impl;

import cl.safe.dao.AdministrarEvaluacionesDao;
import cl.safe.rowMapper.EmpresaComunaMapper;
import cl.safe.rowMapper.EvaluacionMapper;
import cl.safe.rowMapper.RevisionEvaluacionMapper;
import cl.safe.rowMapper.InstalacionEmpresaMapper;
import cl.safe.rowMapper.PersonaIngenieroMapper;
import cl.safe.rowMapper.PersonaTecnicaMapper;
import cl.safe.rowMapper.TecnicoMapper;
import cl.safe.rowMapper.TrabajadorMapper;
import cl.safe.to.EmpresaComuna;
import cl.safe.to.Evaluacion;
import cl.safe.to.EvaluacionRevision;
import cl.safe.to.InstalacionComuna;
import cl.safe.to.PersonaIngeniero;
import cl.safe.to.PersonaTecnico;
import cl.safe.to.RevisionEvaluacionEmpresa;
import cl.safe.to.Tecnico;
import cl.safe.to.Trabajador;
import java.io.StringReader;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class AdministrarEvaluacionesDaoImpl implements AdministrarEvaluacionesDao {
    
    private JdbcTemplate jdbctemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate; 
    
    @Autowired
    @Qualifier("dataSource")
    public void setDatasource(DataSource dataSource){
        jdbctemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    /**
     * Se encarga de obtener la evaluaciones por rut de empresa
     * @param rut
     * @return 
     */
    public List<Evaluacion> obtenerEvaluacionPorRut(int rut) {
        
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT 1 AS \"TIPO_EVALUACION\", ");
        sql.append("EVA.ID_EVALUACION_INSTALACION AS \"ID_EVALUACION\",EVA.FECHA_EVALUACION_INST AS \"FECHA_EVALUACION\" ,EVA.OBSERVACIONES ,  ");
        sql.append("EVA.INFORME_OK,EVA.ETAPA_EVA AS \"ETAPA\",EVA.ID_TECNICO,EVA.ID_INSTALACION , NULL AS \"ID_TRABAJADOR\", C.ID_COMUNA AS \"ID_COMUNA\",  ");
        sql.append("R.ID_REGION AS \"ID_REGION\" FROM EMPRESA E INNER JOIN INSTALACION I ON E.ID_EMPRESA = I.ID_EMPRESA INNER JOIN INSTALACION_COMUNA IC  ");
        sql.append("ON  I.ID_INSTALACION = IC.ID_INSTALACION INNER JOIN COMUNA C ON IC.ID_COMUNA = C.ID_COMUNA INNER JOIN REGION R ON C.REGION_ID_REGION = R.ID_REGION  ");
        sql.append("INNER JOIN EVALUACION_INSTALACION EVA ON I.ID_INSTALACION = EVA.ID_INSTALACION WHERE E.RUT_EMPRESA = ? ");
        sql.append("UNION ALL  ");
        sql.append("SELECT    2 AS TIPO_EVALUACION, EVA.ID_EVALUACION_TRABAJADOR AS ID_EVALUACION, EVA.FECHA_EVALUACION_TRAB AS FECHA_EVALUACION ");
        sql.append(",   EVA.OBSERVACIONES,EVA.INFORME_OK, EVA.ETAPA_EVA AS ETAPA, EVA.ID_TECNICO, NULL AS ID_INSTALACION ");
        sql.append(",   null,   NULL AS ID_COMUNA,NULL AS ID_REGION ");
        sql.append("FROM EMPRESA E INNER JOIN EVALUACION_TRABAJADOR EVA  ON E.ID_EMPRESA = EVA.ID_EMPRESA INNER JOIN REV_EVA_TRABAJADOR REV ");
        sql.append("ON EVA.ID_EVALUACION_TRABAJADOR  = EVA.ID_EVALUACION_TRABAJADOR  ");
        sql.append("WHERE E.RUT_EMPRESA = ? ");

        System.out.println("sql " + sql);
        
        return jdbctemplate.query(sql.toString(),new Object[]{rut,rut} , new EvaluacionMapper());
    }
            
    public Tecnico obtenerTecnico(int id) {
        Tecnico tecnico = new Tecnico();
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT ID_TECNICO,RUT_PERSONA ");
        sql.append("FROM TECNICO ");
        sql.append("WHERE ID_TECNICO = ?");
         
        try {
             tecnico = jdbctemplate.queryForObject(sql.toString(),new Object[]{id}, new TecnicoMapper());
        } catch (EmptyResultDataAccessException e) {
            System.out.println("e : " + e.getMessage());
        }
        return tecnico;
    }
    
    public Trabajador obtenerTrabajadorPorIdTencnico(int id) {
        Trabajador trabajador = new Trabajador();
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT RUT_PERSONA, CARGO_TRABAJADOR ");
        sql.append("FROM TRABAJADOR ");
        sql.append("WHERE ID_TRABAJADOR = ?");
        try {
             trabajador = jdbctemplate.queryForObject(sql.toString(),new Object[]{id}, new TrabajadorMapper());     
        } catch (EmptyResultDataAccessException e) {
            System.out.println("e : " + e.getMessage());
        }
        return trabajador;
    }
    
    public List<PersonaTecnico> listarTecnicos() {
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT p.RUT_PERSONA,p.NOMBRE_PERSONA,p.APEPAT_PERSONA,p.APEMAT_PERSONA ");
        sql.append(",p.EMAIL_PERSONA,p.DIRECCION_PERSONA,p.FECHA_INGRESO,p.CELULAR_PERSONA ");
        sql.append(",p.FECHA_NACIMIENTO,p.ID_COMUNA,ID_EMPRESA, t.ID_TECNICO  ");
        sql.append("FROM PERSONA p ");
        sql.append("inner join TECNICO t ");
        sql.append("ON p.rut_persona = t.rut_persona ");
        return jdbctemplate.query(sql.toString(), new PersonaTecnicaMapper()); 
        
    }
    
    public List<PersonaIngeniero> listarIngenieros() {
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT p.RUT_PERSONA,p.NOMBRE_PERSONA,p.APEPAT_PERSONA,p.APEMAT_PERSONA ");
        sql.append(",p.EMAIL_PERSONA,p.DIRECCION_PERSONA,p.FECHA_INGRESO,p.CELULAR_PERSONA ");
        sql.append(",p.FECHA_NACIMIENTO,p.ID_COMUNA,ID_EMPRESA, i.ID_INGENIERO ");
        sql.append("FROM PERSONA p ");
        sql.append("inner join INGENIERO i ");
        sql.append("ON p.rut_persona = i.rut_persona ");
        return jdbctemplate.query(sql.toString(), new PersonaIngenieroMapper()); 
        
    }
    
    public List<InstalacionComuna> listarInstalacionEmpresas(int rutEmpresa) {
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT i.id_instalacion,i.telefono_instalacion,i.direccion, ");
        sql.append("i.id_empresa, c.id_comuna,c.nombre_comuna,c.region_id_region  ");
        sql.append("FROM INSTALACION i ");
        sql.append("INNER JOIN EMPRESA E ON I.ID_EMPRESA = E.ID_EMPRESA ");
        sql.append("INNER JOIN INSTALACION_COMUNA ic ON i.id_instalacion = ic.id_instalacion  ");
        sql.append("INNER JOIN COMUNA c ON c.id_comuna = ic.id_comuna ");
        sql.append("WHERE E.RUT_EMPRESA = ? ");
        
        return jdbctemplate.query(sql.toString(),new Object[]{rutEmpresa}, new InstalacionEmpresaMapper()); 
        
    }

    public List<EmpresaComuna> listarEmpresasComuna() {
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT e.id_empresa, e.rut_empresa, e.nombre_empresa, e.telefono_empresa, ");
        sql.append("e.direccion_empresa, e.fecha_ingreso, e.rubro_empresa, e.id_comuna, ");
        sql.append("e.estado, e.fecha_termino,e.id_usuario,e.email_empresa,c.nombre_comuna,c.region_id_region ");
        sql.append("FROM EMPRESA e ");
        sql.append("INNER JOIN COMUNA c ");
        sql.append("on c.id_comuna = e.id_comuna ");
        System.out.println("sql " + sql);
        return jdbctemplate.query(sql.toString(), new EmpresaComunaMapper());  
    }

    public Boolean agregarEvaluacionInstalacion(Evaluacion eva){
        System.out.println("eva "+ eva.toString()); 
         Boolean resultado = true;
         try {
             jdbctemplate.update(
                "{ call SP_INSERT_EVA_INSTALACION(?,?,?,?,?,?,?) }",
                1,eva.getFechaEvaluacion(),
                eva.getIdTecnico(),eva.getIdInstaoTraba(),eva.getInformeOk(),
                eva.getEtapaEva(),eva.getObservacion()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado = false;
        }
        return resultado;   
    }
   
    public Boolean modificarEvaluacionInstalacion(Evaluacion eva) {
        Boolean resultado = true;
        try {
             jdbctemplate.update(
                "{ call SP_UPDATE_EVA_INSTALACION(?,?,?,?,?,?,?) }",
                eva.getId(),eva.getFechaEvaluacion(),
                eva.getIdTecnico(),eva.getIdInstaoTraba(),eva.getInformeOk(),
                eva.getEtapaEva(),eva.getObservacion()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado =false;
        }
        return resultado;   
    }

    public Boolean agregarEvaluacionTrabajador(Evaluacion eva) {
        Boolean resultado = true;
        System.out.println("eva " + eva);
        try {
             jdbctemplate.update(
                "{ call SP_INSERT_EVA_TRABAJADOR(?,?,?,?,?,?,?) }",
                eva.getId(),eva.getFechaEvaluacion(),
                eva.getIdTecnico(),eva.getInformeOk(),
                eva.getEtapaEva(),eva.getObservacion(),eva.getIdEmpresa()
            );

        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado =false;
        }
        return resultado;   
    }

    public Boolean modificarEvaluacionTrabajador(Evaluacion eva) {
        Boolean resultado = true;
        try {
             jdbctemplate.update(
                "{ call SP_UPDATE_EVA_TRABAJADOR(?,?,?,?,?,?,?) }",
                eva.getId(),eva.getFechaEvaluacion(),
                eva.getIdTecnico(),eva.getInformeOk(),
                eva.getEtapaEva(),eva.getObservacion(),eva.getIdEmpresa()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado=false;
        }
        return resultado;   
    }
    
    /**
     * Servicios insert and update de revision evaluacion
     */

     public Boolean agregarEvaluacionRevInstalacion(EvaluacionRevision evaRev) {
         Boolean resultado = true;
         System.out.println("evaRev "+evaRev.toString());
         try {
             jdbctemplate.update(
                "{ call SP_INSERT_REV_EVA_INSTALACION(?,?,?,?,?) }",
                evaRev.getId(), evaRev.getIdIngeniero(), evaRev.getIdEvaInstOTrab(),
                evaRev.getRecomendaciones(), evaRev.getIdSupervisor()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado = false;
        }
        return resultado;   
    }
   
    public Boolean modificarEvaluacionRevInstalacion(EvaluacionRevision evaRev) {
        Boolean resultado = true;
        try {
             jdbctemplate.update(
                "{ call SP_UPDATE_REV_EVA_INSTALACION(?,?,?,?,?) }",
                evaRev.getId(), evaRev.getIdIngeniero(), evaRev.getIdEvaInstOTrab(),
                evaRev.getRecomendaciones(),evaRev.getIdSupervisor()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado =false;
        }
        return resultado;   
    }
    
    public Boolean agregarEvaluacionRevTrabajador(EvaluacionRevision evaRev) {
        Boolean resultado = true;
        System.out.println("evaRev "+ evaRev);
        try {
             jdbctemplate.update(
                "{ call SP_INSERT_REV_EVA_TRABAJADOR(?,?,?,?,?) }",
                evaRev.getId(), evaRev.getIdIngeniero(),evaRev.getRecomendaciones()
                ,evaRev.getIdEvaInstOTrab(), evaRev.getIdSupervisor()
            );

        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado = false;
        }
        return resultado;   
    }

    public Boolean modificarEvaluacionRevTrabajador(EvaluacionRevision evaRev) {
        Boolean resultado = true;
        
        try {
             jdbctemplate.update(
                "{ call SP_UPDATE_REV_EVA_TRABAJADOR(?,?,?,?,?) }",
                evaRev.getId(), evaRev.getIdIngeniero(),evaRev.getRecomendaciones()
                ,evaRev.getIdEvaInstOTrab(), evaRev.getIdSupervisor()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            resultado = false;
        }
        return resultado;   
    }
    
   public List<RevisionEvaluacionEmpresa> obtenerRevisionEvaluacionesPorIdEmpresa(int idEvaluacion) {
        
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT  1 AS \"TIPO_EVALUACION\", EVA.ID_EVALUACION_INSTALACION AS \"ID_EVALUACION\",    ");
        sql.append("REV.ID_REV_EVA_INSTALACION AS \"ID_REVISION\", REV.ID_INGENIERO,    ");
        sql.append("REV.RECOMENDACIONES_INSTA AS \"RECOMENDACIONES\", REV.ID_SUPERVISOR, E.ID_EMPRESA ");
        sql.append("FROM EMPRESA E ");
        sql.append("INNER JOIN INSTALACION I ON E.ID_EMPRESA = I.ID_EMPRESA ");
        sql.append("INNER JOIN INSTALACION_COMUNA IC ON  I.ID_INSTALACION = IC.ID_INSTALACION    ");
        sql.append("INNER JOIN COMUNA C ON IC.ID_COMUNA = C.ID_COMUNA ");
        sql.append("INNER JOIN REGION R ON C.REGION_ID_REGION = R.ID_REGION ");
        sql.append("INNER JOIN EVALUACION_INSTALACION EVA ON I.ID_INSTALACION = EVA.ID_INSTALACION ");
        sql.append("INNER JOIN REV_EVA_INSTALACION REV ON EVA.ID_EVALUACION_INSTALACION = REV.ID_EVALUACION_INSTALACION ");
        sql.append("WHERE E.RUT_EMPRESA = ? ");
        sql.append("UNION ALL ");
        sql.append(" SELECT  2 AS \"TIPO_EVALUACION\", EVA.ID_EVALUACION_TRABAJADOR AS \"ID_EVALUACION\",REV.ID_REV_EVA_TRABAJADOR AS \"IDREVISION\", ");
        sql.append("REV.ID_INGENIERO AS \"ID_INGENIERO\",  REV.RECOMENDACIONES_TRAB AS \"RECOMENDACIONES\", REV.ID_SUPERVISOR, E.ID_EMPRESA  ");
        sql.append("FROM EMPRESA E  ");
        sql.append("INNER JOIN EVALUACION_TRABAJADOR EVA   ");
        sql.append("ON E.ID_EMPRESA = EVA.ID_EMPRESA   ");
        sql.append("INNER JOIN REV_EVA_TRABAJADOR REV  ");
        sql.append("ON EVA.ID_EVALUACION_TRABAJADOR  = EVA.ID_EVALUACION_TRABAJADOR   ");
        sql.append("WHERE E.RUT_EMPRESA = ? ");
        
        System.out.println("sql " + sql);
    
        return jdbctemplate.query(sql.toString(),new Object[]{idEvaluacion,idEvaluacion} , new RevisionEvaluacionMapper());
    } 

}
