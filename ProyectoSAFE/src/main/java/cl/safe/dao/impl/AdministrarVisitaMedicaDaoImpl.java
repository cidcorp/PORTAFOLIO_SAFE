
package cl.safe.dao.impl;

import cl.safe.dao.AdministrarVisitaMedicaDao;
import cl.safe.rowMapper.AgendaMedicaMapper;
import cl.safe.rowMapper.AtencionMedicaMapper;
import cl.safe.rowMapper.ExamenMapper;
import cl.safe.rowMapper.FichaMedicaMapper;
import cl.safe.rowMapper.MedicoMapper;
import cl.safe.to.AgendaMedica;
import cl.safe.to.AtencionMedica;
import cl.safe.to.ExamenEmpresa;
import cl.safe.to.ExamenFichaMedica;
import cl.safe.to.FichaMedica;
import cl.safe.to.Medico;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class AdministrarVisitaMedicaDaoImpl implements AdministrarVisitaMedicaDao {
    
    private JdbcTemplate jdbctemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate; 
    
    @Autowired
    @Qualifier("dataSource")
    public void setDatasource(DataSource dataSource){
        jdbctemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
    public List<AgendaMedica> obtenerAgendaMedicaPorRutEmpresa(int rutEmpresa) {
        System.out.println(" rut:  " + rutEmpresa);
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT  ID_AGENDA_MEDICA,FECHA_DISPONIBLE,ID_MEDICO,CONFIRMADO,ID_EMPRESA ");
        sql.append("FROM agenda_medica ");
        sql.append("WHERE  ID_EMPRESA = ? ");
        
        return jdbctemplate.query(sql.toString(),new Object[]{rutEmpresa} , new AgendaMedicaMapper());
    }
    
    public List<ExamenEmpresa> obtenerExamenesEmpresa(int rutEmpresa) {
        System.out.println(" rut:  " + rutEmpresa);
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT ex.ID_EXAMEN,ex.TIPO_EXAMEN,ex.FECHA_EXAMEN, ");
        sql.append("fm.ID_FICHA_MEDICA,e.ID_EMPRESA ");
        sql.append("FROM EMPRESA e, AGENDA_MEDICA am,ATENCION_MEDICA at,FICHA_MEDICA fm,EXAMEN ex ");
        sql.append("where e.ID_EMPRESA=am.ID_EMPRESA AND ");
        sql.append("at.id_atencion_med=fm.id_atencion_med AND ");
        sql.append("fm.id_ficha_medica=ex.id_ficha_medica ");
        sql.append("AND e.ID_EMPRESA = ? ");
        
        return jdbctemplate.query(sql.toString(),new Object[]{rutEmpresa} , new ExamenMapper());
    }
    
            
            
    public List<FichaMedica> obtenerFichaMedica(int rutEmpresa) {
        System.out.println(" rut:  " + rutEmpresa);
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT f.ID_TRABAJADOR,f.ID_ATENCION_MED,f.DIAGNOSTICO, f.ID_FICHA_MEDICA,a.id_empresa ");
        sql.append("FROM ficha_medica f  ");
        sql.append("INNER JOIN atencion_medica a ON f.id_atencion_med = a.id_atencion_med ");
        sql.append("INNER JOIN EMPRESA E on a.id_empresa = e.id_empresa ");
        sql.append("WHERE e.rut_empresa =  ? ");
        
        return jdbctemplate.query(sql.toString(),new Object[]{rutEmpresa} , new FichaMedicaMapper());
    }
    public List<Medico> obtenerMedicos() {

        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT  me.ID_MEDICO,me.RUT_PERSONA,me.ESPECIALIDAD,p.RUT_PERSONA, ");
        sql.append("p.NOMBRE_PERSONA,p.APEPAT_PERSONA,p.APEMAT_PERSONA,p.EMAIL_PERSONA, ");
        sql.append("p.DIRECCION_PERSONA,p.FECHA_INGRESO,p.CELULAR_PERSONA,p.FECHA_NACIMIENTO, ");
        sql.append("p.ID_COMUNA,p.ID_EMPRESA,p.ESTADO,p.TIPO_ROL,p.ID_USUARIO ");
        sql.append("FROM MEDICO me  ");
        sql.append("INNER JOIN PERSONA p ");
        sql.append("ON me.rut_persona = p.rut_persona ");

        return jdbctemplate.query(sql.toString(), new MedicoMapper());
    }
    
    public AgendaMedica obtenerAgendaMedicaPorId(int idAgendaMedica) {

        AgendaMedica agenciaMedica = new AgendaMedica();
                
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT ID_AGENDA_MEDICA,FECHA_DISPONIBLE,ID_MEDICO,CONFIRMADO,ID_EMPRESA ");
        sql.append("FROM AGENDA_MEDICA ");
        sql.append("WHERE ID_AGENDA_MEDICA = ? ");

         try {
            agenciaMedica = jdbctemplate.queryForObject(sql.toString(),new Object[]{idAgendaMedica}, new AgendaMedicaMapper());
        } catch (EmptyResultDataAccessException e) {
            System.out.println("e : " + e.getMessage());
        }
         return agenciaMedica;
    }
    @Override
    public Boolean agregarAgendaMedica(AgendaMedica agenda) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_INSERT_AGENDA_MEDICA (?,?,?,?,?) }",
                    agenda.getId(), agenda.getFechaDisponible(), agenda.getIdMedico(),
                    agenda.getConfirmado(), agenda.getIdEmpresa()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }

    @Override
    public Boolean modificarAgendaMedica(AgendaMedica agenda) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_UPDATE_AGENDA_MEDICA(?,?,?,?,?) }",
                    agenda.getId(), agenda.getFechaDisponible(), agenda.getIdMedico(),
                    agenda.getConfirmado(), agenda.getIdEmpresa()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }
    
    @Override
    public Boolean agregarAtencionMedica(AtencionMedica atencionMedica) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_INSERT_ATENCION_MEDICA (?,?,?,?,?) }",
                    atencionMedica.getId(),atencionMedica.getFechaAtencion(),
                    atencionMedica.getRecomendacionesAM(),atencionMedica.getIdAgendaMedico(),
                    atencionMedica.getIdEmpresa()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }

    @Override
    public Boolean modificarAtencionMedica(AtencionMedica atencionMedica) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_UPDATE_ATENCION_MEDICA(?,?,?,?,?) }",
                    atencionMedica.getId(),atencionMedica.getFechaAtencion(),
                    atencionMedica.getRecomendacionesAM(),atencionMedica.getIdAgendaMedico(),
                    atencionMedica.getIdEmpresa()                
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }
    @Override
    public Boolean agregarFichaMedica(FichaMedica fichaMedica) {
        Boolean resultado = true;
            try {
                System.out.println("fichaMedica " +fichaMedica.toString());
                jdbctemplate.update(
                   "{ call SP_INSERT_FICHA_MEDICA (?,?,?,?) }",
                    fichaMedica.getId(),fichaMedica.getDiagnostico(),
                    fichaMedica.getIdTrabajador(), fichaMedica.getIdAtencionMedica()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }

    @Override
    public Boolean modificarFichaMedica(FichaMedica fichaMedica) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_UPDATE_FICHA_MEDICA(?,?,?,?) }",
                    fichaMedica.getId(),fichaMedica.getDiagnostico(),
                    fichaMedica.getIdTrabajador(), fichaMedica.getIdAtencionMedica()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }
    
    @Override
    public Boolean agregarExamenFichaMedica(ExamenFichaMedica examenFichaMedica) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_INSERT_EXAMEN (?,?,?,?) }",
                    examenFichaMedica.getId(), examenFichaMedica.getTipoExamen(),
                    examenFichaMedica.getFechaExamen(), examenFichaMedica.getIdFichaMedica()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }
    
    @Override
    public Boolean eliminarExamenFichaMedica(int idExamenFichaMedica) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_DELETE_EXAMEN (?) }",
                     idExamenFichaMedica   
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }
    
    public List<AtencionMedica> obtenerAtencionMedicaPorIdAgendaMedica(int idEmpresa) {
        StringBuilder sql = new StringBuilder(100);
        System.out.println("idEmpresa " + idEmpresa);
        sql.append("SELECT atm.id_atencion_med,atm.fecha_atencion,atm.recomendaciones_am, ");
        sql.append("atm.id_agenda_medica,atm.id_empresa ");
        sql.append("FROM AGENDA_MEDICA AM ");
        sql.append("INNER JOIN ATENCION_MEDICA ATM  ");
        sql.append("ON ATM.ID_AGENDA_MEDICA = AM.ID_AGENDA_MEDICA ");
        sql.append("WHERE AM.ID_EMPRESA = ? ");
        System.out.println("sql " + sql);
        try {
               return jdbctemplate.query(sql.toString(),new Object[]{idEmpresa} , new AtencionMedicaMapper());    
        } catch (Exception e) {
            System.out.println("error "+ e.getMessage());
            return null;
        }   
    }
}
