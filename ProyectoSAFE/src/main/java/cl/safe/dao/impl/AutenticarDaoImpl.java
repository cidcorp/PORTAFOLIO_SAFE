package cl.safe.dao.impl;

import cl.safe.dao.AutenticarDao;
import cl.safe.to.Login;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AutenticarDaoImpl implements AutenticarDao{

    private JdbcTemplate jdbctemplate; 
    
    @Autowired
    @Qualifier("dataSource")
    public void setDatasource(DataSource dataSource){
        jdbctemplate = new JdbcTemplate(dataSource);
    }

    public Login autenticarUsuario(Login login) {
        Login pass = new Login();
        System.out.println("Login "+ login.toString());
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT password ");
        sql.append("FROM USUARIO u ");
        sql.append("INNER JOIN PERSONA p ON u.id_usuario = p.id_usuario ");
        sql.append("WHERE p.rut_persona = ? ");
        sql.append("UNION ALL  ");
        sql.append("SELECT password ");
        sql.append("FROM USUARIO u ");
        sql.append("INNER JOIN EMPRESA e ON u.id_usuario = e.id_usuario ");
        sql.append("WHERE e.rut_empresa = ? ");

        try {
            pass = (Login) jdbctemplate.queryForObject(sql.toString(),new Object[]{
                login.getRutUsuario(),login.getRutUsuario()},new BeanPropertyRowMapper(Login.class) );
   
        } catch (EmptyResultDataAccessException e) {
            pass = null;
            System.out.println("e : " + e.getMessage());
        }
        return pass;
    }

    
}
