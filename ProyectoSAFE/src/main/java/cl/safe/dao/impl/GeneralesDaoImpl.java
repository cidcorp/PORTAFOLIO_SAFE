package cl.safe.dao.impl;

import cl.safe.dao.GeneralesDao;
import cl.safe.rowMapper.ComunaRowMapper;
import cl.safe.rowMapper.RegionRowMapper;
import cl.safe.to.Comuna;
import cl.safe.to.Region;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class GeneralesDaoImpl implements GeneralesDao{

   private JdbcTemplate jdbctemplate;
    
    @Autowired
    @Qualifier("dataSource")
    public void setDatasource(DataSource dataSource){
        jdbctemplate = new JdbcTemplate(dataSource);
    }
    
    @Override
    public List<Region> obtenerRegiones() {
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT ID_REGION, NOMBRE_REGION FROM REGION ");
        return jdbctemplate.query(sql.toString(), new RegionRowMapper());
    }

    @Override
    public List<Comuna> obtenerComunaPorRegion(int idRegion) {
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT c.ID_COMUNA, c.NOMBRE_COMUNA FROM REGION re ");
        sql.append("INNER JOIN COMUNA c  ");
        sql.append("ON re.ID_REGION = c.REGION_ID_REGION ");
        sql.append("AND c.region_id_region = ? ");
        
        return jdbctemplate.query(sql.toString(),new Object[]{idRegion}, new ComunaRowMapper());
    }
    
    public Boolean onBd() {
        Boolean result = true;
        StringBuilder sql = new StringBuilder(100);
        sql.append("select count(*) from V$VERSION ");
        try {
            int rowCount = jdbctemplate.queryForObject(sql.toString(),Integer.class);    
        } catch (Exception e) {
            result = false;
        }
        return  result;
    }
    
}
