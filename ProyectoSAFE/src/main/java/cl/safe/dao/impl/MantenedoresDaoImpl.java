
package cl.safe.dao.impl;

import cl.safe.to.Empresa;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import cl.safe.dao.MantenedoresDao;
import cl.safe.rowMapper.AdministradorPersonaMapper;
import cl.safe.rowMapper.AsociacionPerfilMenuMapper;
import cl.safe.rowMapper.MenuRowMapper;
import cl.safe.rowMapper.PerfilRowMapper;
import cl.safe.rowMapper.PersonaMapper;
import cl.safe.rowMapper.SupervisorRowMapper;
import cl.safe.rowMapper.TecnicoMapper;
import cl.safe.rowMapper.UsuarioRowMapper;
import cl.safe.to.Administrador;
import cl.safe.to.AsociacionPerfilMenu;
import cl.safe.to.Instalacion;
import cl.safe.to.Medico;
import cl.safe.to.MenuTo;
import cl.safe.to.Perfil;
import cl.safe.to.Persona;
import cl.safe.to.Supervisor;
import cl.safe.to.Trabajador;
import cl.safe.to.Usuario;
import cl.safe.to.UsuarioAuth;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;


@Repository
public class MantenedoresDaoImpl implements MantenedoresDao {
    
    private JdbcTemplate jdbctemplate;
    
    @Autowired
    @Qualifier("dataSource")
    public void setDatasource(DataSource dataSource){
        jdbctemplate = new JdbcTemplate(dataSource);
    }
    
    public Boolean agregarEmpresa(Empresa emp) {
        Boolean result = true;
        try {
            System.out.println("Empresa "  + emp);
             jdbctemplate.update(
                "{ call SP_INSERT_EMPRESA(?,?,?,?,?,?,?,?,?,?,?,?) }",
                emp.getId(),emp.getRut(),emp.getNombre(),emp.getTelefono(),emp.getDireccion(),
                emp.getFechaIngreso(),emp.getRubro(),emp.getIdComuna(),
                emp.getFechaTermino(),emp.getEstado(),emp.getIdUsuario(),emp.getEmail()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            result = false;
        }
        return result;
    }
    
    @Override
    public Boolean modificarEmpresa(Empresa emp) {
        Boolean result = true;
        try {
            System.out.println("emp "+ emp);
             jdbctemplate.update(
                "{ call SP_UPDATE_EMPRESA(?,?,?,?,?,?,?,?,?) }",
                emp.getId(),emp.getTelefono(),emp.getDireccion(),
                emp.getRubro(),emp.getIdComuna(), emp.getFechaTermino(),
                emp.getEstado(),emp.getIdUsuario(),emp.getEmail()
            );
             
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            result = false;
        }
        return result;
    }
    public Boolean agregarUsuario(Usuario usu) {
            Boolean resultado = true;
            System.out.println("usuario "+ usu.toString());
            try {
                 jdbctemplate.update(
                    "{ call SP_INSERT_USUARIO(?,?,?,?) }",
                   usu.getId(),usu.getPassword(),
                   usu.getEstado(), usu.getIdPerfil()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;   
        }
    

    @Override
    public Boolean modificarUsuario(Usuario usu) {
        Boolean resultado = true;
            try {
                 jdbctemplate.update(
                    "{ call SP_UPDATE_USUARIO(?,?,?,?) }",
                   usu.getId(),usu.getPassword(),
                   usu.getEstado(), usu.getIdPerfil()
                    
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }
    
    public Boolean agregarPersona(Persona per) {
            Boolean resultado = true;
            try {
                 jdbctemplate.update(
                    "{ call SP_INSERT_PERSONA(?,?,?,?,?,?,?,?,?,?,?,?,?,?) }",
                     per.getRut(),per.getNombre(),per.getApepat(),per.getApemat(),
                     per.getEmail(), per.getDireccion(),per.getFechaIngreso(),
                     per.getCelular(),per.getFechaNacimiento(), per.getIdComuna(),
                     per.getIdEmpresa(),per.getEstado(),per.getTipoRol(),per.getIdUsuario()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;   
        }
    

    public Boolean modificarPersona(Persona per) {
        Boolean resultado = true;
            try {
                System.out.println("per "+ per);
                jdbctemplate.update(
                   "{ call SP_UPDATE_PERSONA(?,?,?,?,?,?,?,?,?,?,?,?,?) }",
                 per.getRut(),per.getNombre(),per.getApepat(),per.getApemat(),
                 per.getEmail(), per.getDireccion(),per.getFechaIngreso(),
                 per.getCelular(),per.getFechaNacimiento(), per.getIdComuna(),
                 per.getIdEmpresa(),per.getEstado(),per.getIdUsuario()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }

    @Override
    public Boolean agregarMedico(Medico medico) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_INSERT_MEDICO(?,?,?) }",
                  medico.getId(), medico.getRut(),medico.getEspecialidad()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }

    @Override
    public Boolean modificarMedico(Medico medico) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_UPDATE_MEDICO(?,?,?) }",
                 medico.getId(), medico.getRut(),medico.getEspecialidad()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }

    @Override
    public Boolean agregarTrabajador(Trabajador trabajador) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_INSERT_TRABAJADOR(?,?,?,?) }",
                  trabajador.getId(),trabajador.getCargo(),trabajador.getRut(),
                  trabajador.getIdEmpresa()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;    }

    @Override
    public Boolean modificarTrabajador(Trabajador trabajador) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_UPDATE_TRABAJADOR(?,?,?,?) }",
                  trabajador.getId(),trabajador.getCargo(),trabajador.getRut(),
                  trabajador.getIdEmpresa()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }

    @Override
    public Boolean agregarInstalacion(Instalacion instalacion) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_INSERT_INSTALACION(?,?,?,?) }",
                    instalacion.getId(),instalacion.getTelefono(),instalacion.getDireccion(),
                    instalacion.getIdEmpresa()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }

    @Override
    public Boolean modificarInstalacion(Instalacion instalacion) {
        Boolean resultado = true;
            try {
                jdbctemplate.update(
                   "{ call SP_UPDATE_INSTALACION(?,?,?,?) }",
                    instalacion.getId(),instalacion.getTelefono(),instalacion.getDireccion(),
                    instalacion.getIdEmpresa()
                );

            } catch (Exception e) {
                System.out.println("ERROR: " + e.getMessage());
                resultado=false;
            }
            return resultado;
    }

    @Override
    public List<Supervisor> obtenerSupervisores() {
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT s.ID_SUPERVISOR, p.RUT_PERSONA,p.NOMBRE_PERSONA,p.APEPAT_PERSONA,p.APEMAT_PERSONA, ");
        sql.append("p.EMAIL_PERSONA,p.DIRECCION_PERSONA,p.FECHA_INGRESO,p.CELULAR_PERSONA, ");
        sql.append("p.FECHA_NACIMIENTO,p.ID_COMUNA,p.ID_EMPRESA,p.ESTADO ");
        sql.append("FROM PERSONA p  ");
        sql.append("INNER JOIN SUPERVISOR s ");
        sql.append("ON p.rut_persona = s.rut_persona ");
        
        return jdbctemplate.query(sql.toString(), new SupervisorRowMapper());
    }
    
     public List<Perfil> obtenerPerfiles(){
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT ID_PERFIL,DESCRIPCION_PERFIL ");
        sql.append("FROM PERFIL ");
        
        return jdbctemplate.query(sql.toString(), new PerfilRowMapper()); 
     }

    @Override
    public List<MenuTo> obtenerMenu() {
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT ID_MENU, DESCRIPCION_MENU ");
        sql.append("FROM MENU ");
        
        return jdbctemplate.query(sql.toString(), new MenuRowMapper()); 
    }
    
    @Override
    public List<UsuarioAuth> obtenerUsuario() {
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT U.ID_USUARIO,U.ESTADO,U.ID_PERFIL,1 as TIPO,'Persona' as DESCRIPCION_TIPO, ");
        sql.append("P.RUT_PERSONA,P.NOMBRE_PERSONA,p.apepat_persona, ");
        sql.append("p.apemat_persona,P.EMAIL_PERSONA,P.DIRECCION_PERSONA, ");
        sql.append("p.FECHA_INGRESO,p.celular_persona,NULL AS TELEFONO, ");
        sql.append("P.FECHA_NACIMIENTO,NULL AS RUBRO ");
        sql.append("FROM USUARIO U ");
        sql.append("INNER JOIN PERSONA P ON U.ID_USUARIO = P.ID_USUARIO ");
        sql.append("UNION ALL ");
        sql.append("SELECT U.ID_USUARIO,U.ESTADO,U.ID_PERFIL,2 as TIPO,'Empresa' as DESCRIPCION_TIPO, ");
        sql.append("E.RUT_EMPRESA,E.NOMBRE_EMPRESA,NULL,NULL,E.EMAIL_EMPRESA, ");
        sql.append("E.DIRECCION_EMPRESA,E.FECHA_INGRESO,NULL AS CELULAR,e.telefono_empresa, ");
        sql.append("NULL,E.RUBRO_EMPRESA ");
        sql.append("FROM USUARIO U ");
        sql.append("INNER JOIN EMPRESA E ON U.ID_USUARIO = E.ID_USUARIO ");
        System.out.println("sql .. " + sql);
        return jdbctemplate.query(sql.toString(), new UsuarioRowMapper()); 
    }
    
    @Override
    public Persona obtenerPersona(int rutPersona) {
        StringBuilder sql = new StringBuilder(100);
        Persona persona = new Persona();
        sql.append("SELECT RUT_PERSONA,NOMBRE_PERSONA,APEPAT_PERSONA,APEMAT_PERSONA, ");
        sql.append("EMAIL_PERSONA,DIRECCION_PERSONA,FECHA_INGRESO,CELULAR_PERSONA, ");
        sql.append("FECHA_NACIMIENTO,ID_COMUNA,ID_EMPRESA,ESTADO,TIPO_ROL  ");
        sql.append("FROM persona ");
        sql.append("WHERE RUT_PERSONA = ? ");
        
        try {
             persona = jdbctemplate.queryForObject(sql.toString(),new Object[]{rutPersona}, new PersonaMapper());
        } catch (EmptyResultDataAccessException e) {
            System.out.println("e : " + e.getMessage());
        }
        return persona;
    }
 
      public List<Administrador> obtenerAdministrador() {
        
        StringBuilder sql = new StringBuilder(100);
        sql.append("select p.RUT_PERSONA,p.NOMBRE_PERSONA,p.APEPAT_PERSONA,p.APEMAT_PERSONA, ");
        sql.append("p.EMAIL_PERSONA,p.DIRECCION_PERSONA,p.FECHA_INGRESO,p.CELULAR_PERSONA, ");
        sql.append("p.FECHA_NACIMIENTO,p.ID_COMUNA,p.ID_EMPRESA,p.ESTADO,p.TIPO_ROL,p.ID_USUARIO,ad.id_administrador ");
        sql.append("from administrador ad ");
        sql.append("inner join persona p ");
        sql.append("on p.rut_persona = ad.rut_persona");
        
        return jdbctemplate.query(sql.toString(), new AdministradorPersonaMapper()); 
    }
    
      public List<AsociacionPerfilMenu> obtenerAsociacion_perfil_menu(){
        
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT id_perfil, id_menu  ");
        sql.append("FROM asociacion_perfil_menu ");
        
        return jdbctemplate.query(sql.toString(), new AsociacionPerfilMenuMapper()); 
    }
    
}
