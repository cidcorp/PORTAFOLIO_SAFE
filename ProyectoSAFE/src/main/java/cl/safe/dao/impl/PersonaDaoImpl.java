
package cl.safe.dao.impl;

import cl.safe.dao.PersonaDao;
import cl.safe.rowMapper.PersonaMapper;
import cl.safe.to.Persona;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class PersonaDaoImpl implements PersonaDao {
    
    private JdbcTemplate jdbctemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate; 
    
    @Autowired
    @Qualifier("dataSource")
    public void setDatasource(DataSource dataSource){
        jdbctemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    
//    public String guardar(Persona p) {
//        StringBuilder sql = new StringBuilder(100);
//        sql.append("INSERT INTO safe.persona ");
//        sql.append("VALUES (:id,:nombre,:apellido,:direccion,:telefono)");
//        
//        MapSqlParameterSource params = new MapSqlParameterSource();
//        params.addValue("id", p.getId());
//        params.addValue("nombre", p.getNombre());
//        params.addValue("apellido", p.getApellido());
//        params.addValue("direccion", p.getDireccion());
//        params.addValue("telefono", p.getTelefono());
//        try {
//         namedParameterJdbcTemplate.update(sql.toString(), params);   
//        } catch (Exception e) {
//            System.out.println("ERROR: " + e.getMessage());
//        }
//        return "";
//    }
//
//    
//    public List<Persona> getPersonas(String id) {
//        StringBuilder sql = new StringBuilder(100);
//        sql.append("SELECT * FROM persona");
//        return jdbctemplate.query(sql.toString(), new PersonaMapper());   
//    }

    public Persona obtenerPersonaPorRut(int rut) {
        Persona persona = new Persona();
        System.out.println("rut " + rut);
        StringBuilder sql = new StringBuilder(100);
        sql.append("SELECT RUT_PERSONA,NOMBRE_PERSONA,APEPAT_PERSONA,APEMAT_PERSONA ");
        sql.append(",EMAIL_PERSONA,DIRECCION_PERSONA,FECHA_INGRESO,CELULAR_PERSONA ");
        sql.append(",FECHA_NACIMIENTO,ID_COMUNA,ID_EMPRESA ");
        sql.append("FROM persona ");
        sql.append("WHERE RUT_PERSONA = ?");
        System.out.println("sql " + sql);
        try {
            persona = jdbctemplate.queryForObject(sql.toString(),new Object[]{rut}, new PersonaMapper());    
        } catch (EmptyResultDataAccessException e) {
            System.out.println("e : " + e.getMessage());
        }
        
        
        System.out.println("persona " + persona.toString());
        
        return persona;
        
    }

   
}
