 
package cl.safe.restcontroller;

import cl.safe.service.AdministrarCapacitacionService;
import cl.safe.to.AsistenciaCapacitacion;
import cl.safe.to.Capacitacion;
import cl.safe.to.Expositor;
import cl.safe.to.Trabajador;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
public class AdministrarCapacitacionController {

@Autowired
@Qualifier("capacitacion")
AdministrarCapacitacionService capacitacion;


    @GetMapping("/Capacitaciones/{rutEmpresa}")
    public @ResponseBody ResponseEntity<List<Capacitacion>>
      obtenerCapacitacionPorRutEmpresa(@PathVariable int rutEmpresa) {

        List<Capacitacion> t = capacitacion.obtenerCapacitacionPorRutEmpresa(rutEmpresa);     
        return new ResponseEntity<List<Capacitacion>>(t, HttpStatus.OK);
    }

    @GetMapping("/Trabajador/{rutEmpresa}")
    public @ResponseBody ResponseEntity<List<Trabajador>>
      obtenerTrabajadorPorEmpresa(@PathVariable int rutEmpresa) {
          
        List<Trabajador> t = capacitacion.obtenerTrabajadorPorEmpresa(rutEmpresa);
        return new ResponseEntity<List<Trabajador>>(t, HttpStatus.OK);
    }
    
    @PostMapping(path="/Capacitacion",consumes="application/json")
    public ResponseEntity<Boolean> MantenedorCapacitacion(@RequestBody Capacitacion cap) {
        Boolean resultado = capacitacion.MantenedorCapacitacion(cap);
        
        return new ResponseEntity<Boolean>(resultado, HttpStatus.OK);
         
    }
   
    @GetMapping("/asistencia/eliminar/{idAsistencia}")
    public @ResponseBody ResponseEntity<Boolean>
      eliminarAsistenciaMedica(@PathVariable int idAsistencia) {
      Boolean respuesta = capacitacion.eliminarAsistenciaMedica(idAsistencia);
      return new ResponseEntity<Boolean>(respuesta, HttpStatus.OK);
    }

    @GetMapping("/Expositores")
    public @ResponseBody ResponseEntity<List<Expositor>>
      obtenerExpositores() {
        List<Expositor> resultado = capacitacion.obtenerExpositores();
        return new ResponseEntity<List<Expositor>>(resultado, HttpStatus.OK);
    }
    
    @GetMapping("/AsistenciaCapacitaciones/{idCapacitacion}")
    public @ResponseBody ResponseEntity<List<AsistenciaCapacitacion>>
      obtenerAsistenciaCapacitacionPorId(@PathVariable int idCapacitacion) {

        List<AsistenciaCapacitacion> t = capacitacion.obtenerAsistenciaCapacitacionPorId(idCapacitacion);     
        return new ResponseEntity<List<AsistenciaCapacitacion>>(t, HttpStatus.OK);
    }
      
    
    
    @PostMapping(path="/InsertarAsistenciaCapacitacion",consumes="application/json")
    public ResponseEntity<Boolean> insertarAsistenciaCapacitacion(@RequestBody AsistenciaCapacitacion asistencia) {
        Boolean resultado = capacitacion.agregarAsistenciaCapacitacion(asistencia);
        
         return new ResponseEntity<Boolean>(resultado, HttpStatus.OK);
         
    }
    
}
