package cl.safe.restcontroller;

import cl.safe.service.AdministrarEvaluacionesService;
import cl.safe.to.EmpresaComuna;
import cl.safe.to.Evaluacion;
import cl.safe.to.EvaluacionRevision;
import cl.safe.to.InstalacionComuna;
import cl.safe.to.Medico;
import cl.safe.to.PersonaIngeniero;
import cl.safe.to.PersonaTecnico;
import cl.safe.to.RevisionEvaluacionEmpresa;
import cl.safe.to.Tecnico;
import cl.safe.to.Trabajador;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
public class AdministrarEvaluacionesController {

@Autowired
@Qualifier("evalua")
AdministrarEvaluacionesService eval;


    @GetMapping("/Evaluacion/{rutEmpresa}")
    public @ResponseBody ResponseEntity<List<Evaluacion>>
      obtenerEvaluacionPorRut(@PathVariable int rutEmpresa) {

        List<Evaluacion> t = eval.obtenerEvaluacionPorRut(rutEmpresa);     
        return new ResponseEntity<List<Evaluacion>>(t, HttpStatus.OK);
    }

    @GetMapping("/Tecnico/ID/{idTecnico}")
    public @ResponseBody ResponseEntity<Tecnico>
      obtenerTecnico(@PathVariable int idTecnico) {

        Tecnico t = eval.obtenerTecnico(idTecnico);     
        return new ResponseEntity<Tecnico>(t, HttpStatus.OK);
    }
      
    @GetMapping("/Trabajador/ID/{idTecnico}")
    public @ResponseBody ResponseEntity<Trabajador>
      obtenerTrabajadorPorIdTencnico(@PathVariable int idTecnico) {

        Trabajador t = eval.obtenerTrabajadorPorIdTencnico(idTecnico);     
        return new ResponseEntity<Trabajador>(t, HttpStatus.OK);
    }
      
    @GetMapping("/Tecnico")
    public @ResponseBody ResponseEntity<List<PersonaTecnico>>
      listarTecnicos() {
        List<PersonaTecnico> t = eval.listarTecnicos();     
        return new ResponseEntity<List<PersonaTecnico>>(t, HttpStatus.OK);
    }
      
    @GetMapping("/Ingenieros")
    public @ResponseBody ResponseEntity<List<PersonaIngeniero>>
      listarIngenieros() {
        List<PersonaIngeniero> t = eval.listarIngenieros();     
        return new ResponseEntity<List<PersonaIngeniero>>(t, HttpStatus.OK);
    }  
    
    @GetMapping("/InstalacionEmpresa/{rutEmpresa}")
    public @ResponseBody ResponseEntity<List<InstalacionComuna>>
        listarInstalacionEmpresas(@PathVariable int rutEmpresa) {
        List<InstalacionComuna> instEmp = eval.listarInstalacionEmpresas(rutEmpresa);     
        return new ResponseEntity<List<InstalacionComuna>>(instEmp, HttpStatus.OK);
    }

    @GetMapping("/Empresa")
    public @ResponseBody ResponseEntity<List<EmpresaComuna>>
        listarEmpresasComuna() {
        List<EmpresaComuna> emc = eval.listarEmpresasComuna();     
        return new ResponseEntity<List<EmpresaComuna>>(emc, HttpStatus.OK);
    }
        
    @PostMapping(path="/AddAndUpdateEvaluacion",consumes="application/json")
    public ResponseEntity<Boolean> AddAndUpdateEvaluacion(@RequestBody Evaluacion eva) {
         
         Boolean resultado = eval.addAndUpdateEvaluacion(eva);
         if(resultado != null){
            return new ResponseEntity<Boolean>(resultado, HttpStatus.OK);    
         }else{
            return new ResponseEntity<Boolean>(resultado, HttpStatus.BAD_REQUEST); 
         }
         
    }
    
    @PostMapping(path="/AddAndUpdateRevEvaluacion",consumes="application/json")
    public ResponseEntity<Boolean> addAndUpdateRevEvaluacion(@RequestBody EvaluacionRevision eva) {
    
         Boolean resultado = eval.addAndUpdateRevEvaluacion(eva);
         if(resultado != null){
            return new ResponseEntity<Boolean>(resultado, HttpStatus.OK);
         }else{
            return new ResponseEntity<Boolean>(resultado, HttpStatus.BAD_REQUEST); 
         }
    }

    @GetMapping("/EvaluacionRevision/{idEmpresa}")
    public @ResponseBody ResponseEntity<List<RevisionEvaluacionEmpresa>>
      obtenerRevisionEvaluacionesPorIdEmpresa(@PathVariable int idEmpresa) {

        List<RevisionEvaluacionEmpresa> t = eval.obtenerRevisionEvaluacionesPorIdEmpresa(idEmpresa);     
        return new ResponseEntity<List<RevisionEvaluacionEmpresa>>(t, HttpStatus.OK);
    }
      
}
