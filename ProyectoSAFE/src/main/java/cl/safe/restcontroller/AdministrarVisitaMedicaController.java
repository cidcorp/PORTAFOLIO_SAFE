
package cl.safe.restcontroller;


import cl.safe.service.AdministrarVisitaMedicaService;
import cl.safe.to.AgendaMedica;
import cl.safe.to.AtencionMedica;
import cl.safe.to.ExamenEmpresa;
import cl.safe.to.ExamenFichaMedica;
import cl.safe.to.FichaMedica;
import cl.safe.to.Medico;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
public class AdministrarVisitaMedicaController {

@Autowired
@Qualifier("visitaMedica")
AdministrarVisitaMedicaService visitaMedica;


    @GetMapping("/AgendaMedica/{rutEmpresa}")
    public @ResponseBody ResponseEntity<List<AgendaMedica>>
      obtenerAgendaMedicaPorRutEmpresa(@PathVariable int rutEmpresa) {

        List<AgendaMedica> t = visitaMedica.obtenerAgendaMedicaPorRutEmpresa(rutEmpresa);     
        return new ResponseEntity<List<AgendaMedica>>(t, HttpStatus.OK);
    }

    @GetMapping("/ExamenEmpresa/{rutEmpresa}")
    public @ResponseBody ResponseEntity<List<ExamenEmpresa>>
      obtenerExamenesEmpresa(@PathVariable int rutEmpresa) {

       List<ExamenEmpresa> ficha = visitaMedica.obtenerExamenesEmpresa(rutEmpresa);     
       return new ResponseEntity<List<ExamenEmpresa>>(ficha, HttpStatus.OK);
    }
      
    @GetMapping("/FichaMedica/{rutEmpresa}")
    public @ResponseBody ResponseEntity<List<FichaMedica>>
      obtenerFichaMedica(@PathVariable int rutEmpresa) {

        List<FichaMedica> ficha = visitaMedica.obtenerFichaMedica(rutEmpresa);     
        return new ResponseEntity<List<FichaMedica>>(ficha, HttpStatus.OK);
    }
    
    @GetMapping("/Medicos")
    public @ResponseBody ResponseEntity<List<Medico>>
      obtenerMedicos() {

      List<Medico> medico = visitaMedica.obtenerMedicos();     
      return new ResponseEntity<List<Medico>>(medico, HttpStatus.OK);
    }
      
    @GetMapping("/AgendaMedica/id/{idAgendaMedica}")
    public @ResponseBody ResponseEntity<AgendaMedica>
      obtenerAgendaMedicaPorId(@PathVariable int idAgendaMedica) {

      AgendaMedica agendaMedica = visitaMedica.obtenerAgendaMedicaPorId(idAgendaMedica);     
      return new ResponseEntity<AgendaMedica>(agendaMedica, HttpStatus.OK);
    }  
    
    /**
     * Metodos encargado de guardar y modificar agenda medica
     */ 
    @PostMapping(path="/AgendaMedica",consumes="application/json")
    public ResponseEntity<Boolean> mantenedorAgendaMedica(@RequestBody AgendaMedica agenda) {
        String resultado ="";
        Boolean respuesta = visitaMedica.mantenedorAgendaMedica(agenda);     
         return new ResponseEntity<Boolean>(respuesta, HttpStatus.OK);
         
    }
    
    /**
     * Metodos encargado de guardar y modificar Atencion Medica
     */ 
    @PostMapping(path="/AtencionMedica",consumes="application/json")
    public ResponseEntity<Boolean> mantenedorAtencionMedica(@RequestBody AtencionMedica atencionMedica) {
        String resultado ="";
        Boolean respuesta = visitaMedica.mantenedorAtencionMedica(atencionMedica);     
         return new ResponseEntity<Boolean>(respuesta, HttpStatus.OK);
         
    }
      
    /**
     * Metodos Encargado de Guardar y Modificar Ficha Medica
     */ 
    @PostMapping(path="/FichaMedica",consumes="application/json")
    public ResponseEntity<Boolean> mantenedorFichaMedica(@RequestBody FichaMedica fichaMedica) {
        String resultado ="";
        Boolean respuesta = visitaMedica.mantenedorFichaMedica(fichaMedica);     
         return new ResponseEntity<Boolean>(respuesta, HttpStatus.OK);
         
    }
    
    /**
     * Metodos Encargado de Guardar y Modificar Examen Ficha Medica
     */ 
    @PostMapping(path="/AgregarExamenFichaMedica",consumes="application/json")
    public ResponseEntity<Boolean> agregarExamenFichaMedica(@RequestBody ExamenFichaMedica examenFichaMedica) {
        String resultado ="";
        Boolean respuesta = visitaMedica.agregarExamenFichaMedica(examenFichaMedica);     
         return new ResponseEntity<Boolean>(respuesta, HttpStatus.OK);
         
    }
    
    @GetMapping("/eliminarExamenFichaMedica/{id}")
    public @ResponseBody ResponseEntity<Boolean>
        eliminarExamenFichaMedica(@PathVariable int id) {
        Boolean respuesta = visitaMedica.eliminarExamenFichaMedica(id);     
        return new ResponseEntity<Boolean>(respuesta, HttpStatus.OK);  
      }
        
    @GetMapping("/AtencionMedica/{idEmpresa}")
    public @ResponseBody ResponseEntity<List<AtencionMedica>>
      obtenerAtencionMedicaPorIdAgendaMedica(@PathVariable int idEmpresa) {

        List<AtencionMedica> t = visitaMedica.obtenerAtencionMedicaPorIdAgendaMedica(idEmpresa);     
        return new ResponseEntity<List<AtencionMedica>>(t, HttpStatus.OK);
    }   
}
