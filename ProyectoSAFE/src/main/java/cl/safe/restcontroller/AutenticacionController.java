
package cl.safe.restcontroller;


import cl.safe.service.AutenticacionService;
import cl.safe.to.Login;
import cl.safe.to.TokenTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
public class AutenticacionController {

    @Autowired
    @Qualifier("aut")
    AutenticacionService aut;  
    
    @PostMapping(path="/Autenticar",consumes="application/json")
    public ResponseEntity<TokenTo> autenticarUsuario(@RequestBody Login login) {
        System.out.println("Login " + login.toString());
        TokenTo token = aut.autenticarUsuario(login);
        if(token != null) {
            return new ResponseEntity<TokenTo>(token, HttpStatus.OK);
        }else{
            return new ResponseEntity<TokenTo>(token, HttpStatus.UNAUTHORIZED);    
        }
        
        
         
    }
    /**
     * eliminar antes de entregar
     * @param authorization
     * @return 
     */
    @GetMapping("/Header")
    public @ResponseBody ResponseEntity<String>
      obtenerHeader(@RequestHeader("Authorization") String authorization) {

          String header = authorization;      
        return new ResponseEntity<String>(header, HttpStatus.OK);
    }
    
    @PostMapping(path="/validarToken",consumes="application/json")
    public ResponseEntity<Boolean> validarToken(@RequestBody String token, 
        @RequestHeader("Authorization") String authorization) {
        
        Boolean esValido = aut.validarToken(authorization);
        if(esValido){
            return new ResponseEntity<Boolean>(esValido, HttpStatus.OK);
        }else{
            return new ResponseEntity<Boolean>(esValido, HttpStatus.UNAUTHORIZED);    
        }
        
         
    }
   
}
