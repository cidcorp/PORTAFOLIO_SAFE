
package cl.safe.restcontroller;

import cl.safe.service.GeneralesService;
import cl.safe.to.Comuna;
import cl.safe.to.Region;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
public class GeneralesControler {
    
@Autowired
@Qualifier("generalesService")
GeneralesService generalesService;
    
    @GetMapping("/Region")
    public @ResponseBody ResponseEntity<List<Region>>
      obtenerRegiones() {
        List<Region> regiones = generalesService.obtenerRegiones();     
        return new ResponseEntity<List<Region>>(regiones, HttpStatus.OK);
    }

    @GetMapping("/Region/{idRegion}/Comuna")
    public @ResponseBody ResponseEntity<List<Comuna>>
      obtenerComunaPorRegion(@PathVariable int idRegion) {
        List<Comuna> comunas = generalesService.obtenerComunaPorRegion(idRegion);     
        return new ResponseEntity<List<Comuna>>(comunas, HttpStatus.OK);
    }  
    
    @GetMapping("/onServide")
    public @ResponseBody ResponseEntity<String>
      onServide() {
        return new ResponseEntity<String>("Encendido", HttpStatus.OK);
    }
    @GetMapping("/onBD")
    public @ResponseBody ResponseEntity<Boolean>
      onBd() {
          boolean ok = generalesService.onBd();     
        return new ResponseEntity<Boolean>(ok, HttpStatus.OK);
    }
}
