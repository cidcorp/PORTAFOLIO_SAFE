
package cl.safe.restcontroller;

import cl.safe.service.AutenticacionService;
import cl.safe.service.MantenedoresService;
import cl.safe.to.Administrador;
import cl.safe.to.AsociacionPerfilMenu;
import cl.safe.to.Empresa;
import cl.safe.to.Instalacion;
import cl.safe.to.Medico;
import cl.safe.to.MenuTo;
import cl.safe.to.Perfil;
import cl.safe.to.Persona;
import cl.safe.to.Supervisor;
import cl.safe.to.Trabajador;
import cl.safe.to.Usuario;
import cl.safe.to.UsuarioAuth;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;


@org.springframework.web.bind.annotation.RestController
public class MantenedoresController {

@Autowired
@Qualifier("mantenedores")
MantenedoresService mantenedoresService;
AutenticacionService auth = new AutenticacionService();

    @PostMapping(path="/Empresa",consumes="application/json")
    public ResponseEntity<Boolean> MantenedorEmpresa(@RequestBody Empresa emp) {
        Boolean result =true;
        result = mantenedoresService.mantenedorEmpresa(emp);     
        return new ResponseEntity<Boolean>(result, HttpStatus.OK);
         
    }
    
     @PostMapping(path="/Persona",consumes="application/json")
    public ResponseEntity<Boolean> mantenedorPersona(@RequestBody Persona persona) {
        Boolean result = mantenedoresService.mantenedorPersona(persona);
        
         return new ResponseEntity<Boolean>(result, HttpStatus.OK);
         
    }
   @PostMapping(path="/Usuario",consumes="application/json")
    public ResponseEntity<Boolean> modificarCapacitacion(@RequestBody Usuario usu) {
        Boolean result = mantenedoresService.mantenedorUsuario(usu);
        
         return new ResponseEntity<Boolean>(result, HttpStatus.OK);
         
    }
    
   @PostMapping(path="/Medico",consumes="application/json")
    public ResponseEntity<Boolean> mantenedorMedico(@RequestBody Medico medico) {
        Boolean result = mantenedoresService.mantenedorMedico(medico);
        
         return new ResponseEntity<Boolean>(result, HttpStatus.OK);
         
    } 
   
    @PostMapping(path="/Trabajador",consumes="application/json")
    public ResponseEntity<Boolean> mantenedorTrabajador(@RequestBody Trabajador trabajador) {
        Boolean result = mantenedoresService.mantenedorTrabajador(trabajador);
        
         return new ResponseEntity<Boolean>(result, HttpStatus.OK);
         
    }
    
    @PostMapping(path="/Instalacion",consumes="application/json")
    public ResponseEntity<Boolean> mantenedorInstalacion(@RequestBody Instalacion instalacion) {
        Boolean result = mantenedoresService.mantenedorInstalacion(instalacion);
        
         return new ResponseEntity<Boolean>(result, HttpStatus.OK);
         
    }
    @GetMapping("/Supervisor")
    public @ResponseBody ResponseEntity<List<Supervisor>>
      obtenerSupervisores() {
        List<Supervisor> regiones = mantenedoresService.obtenerSupervisores();     
        return new ResponseEntity<List<Supervisor>>(regiones, HttpStatus.OK);
    }
      
    @GetMapping("/Perfiles")
    public @ResponseBody ResponseEntity<List<Perfil>>
       obtenerPerfiles() {
        List<Perfil> regiones = mantenedoresService.obtenerPerfiles();     
        return new ResponseEntity<List<Perfil>>(regiones, HttpStatus.OK);
        
     }
       
    @GetMapping("/Menu")
    public @ResponseBody ResponseEntity<List<MenuTo>>
       obtenerMenu(/*@RequestHeader("Authorization") String token*/) {
         List<MenuTo> menu = new ArrayList<MenuTo>();
        //   if(auth.validarToken(token)){
              menu = mantenedoresService.obtenerMenu();     
            return new ResponseEntity<List<MenuTo>>(menu, HttpStatus.OK);
//        } 
//         else {
//            return new ResponseEntity<List<MenuTo>>(menu, HttpStatus.UNAUTHORIZED);
//         }  
        
     } 
       
     @GetMapping("/Usuario")
     public @ResponseBody ResponseEntity<List<UsuarioAuth>>
       obtenerUsuario() {
        List<UsuarioAuth> usuario = mantenedoresService.obtenerUsuario();     
        return new ResponseEntity<List<UsuarioAuth>>(usuario, HttpStatus.OK);   
     }  
       
     @GetMapping("/Persona/{rutPersona}")
     public @ResponseBody ResponseEntity<Persona>
       obtenerPersona(@PathVariable int rutPersona) {
        Persona persona = mantenedoresService.obtenerPersona(rutPersona);  
        return new ResponseEntity<Persona>(persona, HttpStatus.OK);   
     }
       
     @GetMapping("/Administrador")
     public @ResponseBody ResponseEntity<List<Administrador>>
       obtenerAdministrador() {
        List<Administrador> administradores = mantenedoresService.obtenerAdministrador();     
        return new ResponseEntity<List<Administrador>>(administradores, HttpStatus.OK);
        
     }
       
     @GetMapping("/AsociacionPerfilMenu")
     public @ResponseBody ResponseEntity<List<AsociacionPerfilMenu>>
       obtenerAsociacion_perfil_menu() {
        List<AsociacionPerfilMenu> asoPerfilMenu = mantenedoresService.obtenerAsociacion_perfil_menu();     
        return new ResponseEntity<List<AsociacionPerfilMenu>>(asoPerfilMenu, HttpStatus.OK);
        
     }  
}
