package cl.safe.rowMapper;

import cl.safe.to.Administrador;
import cl.safe.to.Expositor;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdministradorPersonaMapper implements RowMapper<Administrador>{

    public Administrador mapRow(ResultSet rs, int rowNum) throws SQLException {
        Administrador adm = new Administrador();
        adm.setRut(rs.getInt("RUT_PERSONA"));
        adm.setNombre(rs.getString("NOMBRE_PERSONA"));
        adm.setApepat(rs.getString("APEPAT_PERSONA"));
        adm.setApemat(rs.getString("APEMAT_PERSONA"));
        adm.setEmail(rs.getString("EMAIL_PERSONA"));
        adm.setDireccion(rs.getString("DIRECCION_PERSONA"));
        adm.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
        adm.setCelular(rs.getString("CELULAR_PERSONA"));
        adm.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
        adm.setIdComuna(rs.getInt("ID_COMUNA"));
        adm.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        adm.setEstado(rs.getString("ESTADO"));
        adm.setId(rs.getInt("ID_ADMINISTRADOR"));
        
        return adm;
    }
}
