package cl.safe.rowMapper;

import cl.safe.to.AgendaMedica;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AgendaMedicaMapper implements RowMapper<AgendaMedica>{

    public AgendaMedica mapRow(ResultSet rs, int rowNum) throws SQLException {
        AgendaMedica agenda = new AgendaMedica();
        agenda.setId(rs.getInt("ID_AGENDA_MEDICA"));
        agenda.setFechaDisponible(rs.getDate("FECHA_DISPONIBLE"));
        agenda.setIdMedico(rs.getInt("ID_MEDICO"));
        agenda.setConfirmado(rs.getString("CONFIRMADO"));
        int idEmpresa = rs.getInt("ID_EMPRESA");
        if(idEmpresa > 0){
            agenda.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        }
        
        return agenda;
    }
}
