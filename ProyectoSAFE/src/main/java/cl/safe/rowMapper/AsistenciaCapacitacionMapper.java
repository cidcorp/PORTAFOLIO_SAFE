package cl.safe.rowMapper;

import cl.safe.to.AsistenciaCapacitacion;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AsistenciaCapacitacionMapper implements RowMapper<AsistenciaCapacitacion>{

    public AsistenciaCapacitacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        AsistenciaCapacitacion asisCap = new AsistenciaCapacitacion();
        asisCap.setId(rs.getInt("ID_CAPACITACION"));
        asisCap.setFecha(rs.getDate("FECHA"));
        asisCap.setRegistro(rs.getInt("REGISTRO_ASISTENCIA"));
        asisCap.setIdTrabajador(rs.getInt("ID_TRABAJADOR"));
        
        return asisCap;
    }
}
