package cl.safe.rowMapper;


import cl.safe.to.AsociacionPerfilMenu;
import cl.safe.to.ExamenEmpresa;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AsociacionPerfilMenuMapper implements RowMapper<AsociacionPerfilMenu>{

    public AsociacionPerfilMenu mapRow(ResultSet rs, int rowNum) throws SQLException {
        AsociacionPerfilMenu apm = new AsociacionPerfilMenu();
        apm.setIdPerfil(rs.getInt("id_perfil"));
        apm.setIdMenu(rs.getInt("id_menu"));
        return apm;
    }
}
