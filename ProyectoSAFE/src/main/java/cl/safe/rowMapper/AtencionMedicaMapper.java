package cl.safe.rowMapper;

import cl.safe.to.AtencionMedica;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AtencionMedicaMapper implements RowMapper<AtencionMedica>{

    public AtencionMedica mapRow(ResultSet rs, int rowNum) throws SQLException {
        AtencionMedica atencion = new AtencionMedica();
        atencion.setId(rs.getInt("id_atencion_med"));
        atencion.setFechaAtencion(rs.getDate("fecha_atencion"));
        atencion.setRecomendacionesAM(rs.getString("recomendaciones_am"));
        atencion.setIdAgendaMedico(rs.getInt("id_agenda_medica"));
        atencion.setIdEmpresa(rs.getInt("id_empresa"));
        return atencion;
    }
}
