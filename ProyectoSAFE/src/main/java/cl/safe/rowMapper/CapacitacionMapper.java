package cl.safe.rowMapper;

import cl.safe.to.Capacitacion;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CapacitacionMapper implements RowMapper<Capacitacion>{

    public Capacitacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        Capacitacion c = new Capacitacion();
        c.setId(rs.getInt("ID_CAPACITACION"));
        c.setAsunto(rs.getString("ASUNTO_CAPACITACION"));
        c.setFechaInicio(rs.getDate("FECHA_INICIO"));
        c.setFechaTermino(rs.getDate("FECHA_TERMINO"));
        c.setCupoMinimo(rs.getInt("CUPO_MINIMO"));
        c.setIdSupervisor(rs.getInt("ID_SUPERVISOR"));
        c.setIdExpositor(rs.getInt("ID_EXPOSITOR"));
        c.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        c.setCantidadAsistencia(rs.getInt("CANTIDAD_ASISTENTES"));

        return c;
    }
}
