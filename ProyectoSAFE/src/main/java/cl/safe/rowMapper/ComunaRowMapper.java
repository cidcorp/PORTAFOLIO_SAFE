
package cl.safe.rowMapper;

import cl.safe.to.Comuna;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ComunaRowMapper implements RowMapper<Comuna>{

    public Comuna mapRow(ResultSet rs, int rowNum) throws SQLException {
        Comuna comuna = new Comuna();
        comuna.setId(rs.getInt("ID_COMUNA"));
        comuna.setNombre(rs.getString("NOMBRE_COMUNA"));
        return comuna;
    }
}
