
package cl.safe.rowMapper;

import cl.safe.to.EmpresaComuna;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;


public class EmpresaComunaMapper implements RowMapper<EmpresaComuna>{

    public EmpresaComuna mapRow(ResultSet rs, int rowNum) throws SQLException {
        EmpresaComuna ec = new EmpresaComuna();
        
        ec.getEmpresa().setId(rs.getInt("id_empresa"));
        ec.getEmpresa().setRut(rs.getInt("rut_empresa"));
        ec.getEmpresa().setNombre(rs.getString("nombre_empresa"));
        ec.getEmpresa().setTelefono(rs.getInt("telefono_empresa"));
        ec.getEmpresa().setDireccion(rs.getString("direccion_empresa"));
        ec.getEmpresa().setFechaIngreso(rs.getDate("fecha_ingreso"));
        ec.getEmpresa().setRubro(rs.getString("rubro_empresa"));
        ec.getComuna().setId(rs.getInt("id_comuna"));
        ec.getEmpresa().setEmail(rs.getString("email_empresa"));
        ec.getEmpresa().setEstado(rs.getString("estado"));
        ec.getEmpresa().setFechaTermino(rs.getDate("fecha_termino"));
        ec.getComuna().setNombre(rs.getString("nombre_comuna"));
        ec.getRegion().setId(rs.getInt("region_id_region"));
        
        return ec;
    }
}
