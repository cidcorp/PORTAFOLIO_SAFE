package cl.safe.rowMapper;

import cl.safe.to.Evaluacion;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class EvaluacionMapper implements RowMapper<Evaluacion>{

    public Evaluacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        Evaluacion eva = new Evaluacion();
        eva.setTipoEvaluacion(rs.getInt("TIPO_EVALUACION"));
        eva.setId(rs.getInt("ID_EVALUACION"));
        eva.setFechaEvaluacion(rs.getDate("FECHA_EVALUACION"));
        eva.setObservacion(rs.getString("OBSERVACIONES"));
        eva.setInformeOk(rs.getInt("INFORME_OK"));
        eva.setEtapaEva(rs.getInt("ETAPA"));
        eva.setIdTecnico(rs.getInt("ID_TECNICO"));
        int valor = rs.getInt("ID_INSTALACION");
        if(valor == 0){
            eva.setIdInstaoTraba(rs.getInt("ID_TRABAJADOR"));
        }else{
            eva.setIdInstaoTraba(rs.getInt("ID_INSTALACION"));
        }
        eva.setIdComuna(rs.getInt("ID_COMUNA"));
        eva.setIdRegion(rs.getInt("ID_REGION"));
        return eva;
    }
}
