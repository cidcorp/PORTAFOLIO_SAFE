package cl.safe.rowMapper;


import cl.safe.to.ExamenEmpresa;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ExamenMapper implements RowMapper<ExamenEmpresa>{

    public ExamenEmpresa mapRow(ResultSet rs, int rowNum) throws SQLException {
        ExamenEmpresa examen = new ExamenEmpresa();
        examen.setId(rs.getInt("ID_EXAMEN"));
        examen.setTipoExamen(rs.getString("TIPO_EXAMEN"));
        examen.setFechaExamen(rs.getDate("FECHA_EXAMEN"));
        examen.setIdFichaMedica(rs.getString("ID_FICHA_MEDICA").charAt(0));
        examen.setIdEmpresa(rs.getInt("ID_EMPRESA"));

        return examen;
    }
}
