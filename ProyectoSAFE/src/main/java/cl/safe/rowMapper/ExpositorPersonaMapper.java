package cl.safe.rowMapper;

import cl.safe.to.Expositor;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ExpositorPersonaMapper implements RowMapper<Expositor>{

    public Expositor mapRow(ResultSet rs, int rowNum) throws SQLException {
        Expositor exp = new Expositor();
        exp.setRut(rs.getInt("RUT_PERSONA"));
        exp.setNombre(rs.getString("NOMBRE_PERSONA"));
        exp.setApepat(rs.getString("APEPAT_PERSONA"));
        exp.setApemat(rs.getString("APEMAT_PERSONA"));
        exp.setEmail(rs.getString("EMAIL_PERSONA"));
        exp.setDireccion(rs.getString("DIRECCION_PERSONA"));
        exp.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
        exp.setCelular(rs.getString("CELULAR_PERSONA"));
        exp.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
        exp.setIdComuna(rs.getInt("ID_COMUNA"));
        exp.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        exp.setEstado(rs.getString("ESTADO"));
        exp.setId(rs.getInt("ID_EXPOSITOR"));
        

        return exp;
    }
}
