package cl.safe.rowMapper;

import cl.safe.to.FichaMedica;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FichaMedicaMapper implements RowMapper<FichaMedica>{

    public FichaMedica mapRow(ResultSet rs, int rowNum) throws SQLException {
        FichaMedica fichaMedica = new FichaMedica();
        fichaMedica.setId(rs.getInt("ID_FICHA_MEDICA"));
        fichaMedica.setIdTrabajador(rs.getInt("ID_TRABAJADOR"));
        fichaMedica.setIdAtencionMedica(rs.getInt("ID_ATENCION_MED"));
        fichaMedica.setDiagnostico(rs.getString("DIAGNOSTICO"));
        fichaMedica.setIdEmpresa(rs.getInt("id_empresa"));
                
        return fichaMedica;
    }
}
