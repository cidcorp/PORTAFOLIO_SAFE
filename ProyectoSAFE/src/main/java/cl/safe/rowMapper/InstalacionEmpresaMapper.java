
package cl.safe.rowMapper;

import cl.safe.to.InstalacionComuna;;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;


public class InstalacionEmpresaMapper implements RowMapper<InstalacionComuna>{

    public InstalacionComuna mapRow(ResultSet rs, int rowNum) throws SQLException {
        InstalacionComuna ic = new InstalacionComuna();
        ic.setIdInstalacion(rs.getInt("id_instalacion"));
        ic.setTelefonoInstalacion(rs.getInt("telefono_instalacion"));
        ic.setDireccionIntalacion(rs.getString("direccion"));
        ic.setIdEmpresa(rs.getInt("id_empresa"));
        ic.setIdComuna(rs.getInt("id_comuna"));
        ic.setNombreComuna(rs.getString("nombre_comuna"));
        ic.setIdRegion(rs.getInt("region_id_region"));
        
        return ic;
    }
}
