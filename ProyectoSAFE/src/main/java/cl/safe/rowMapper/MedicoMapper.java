package cl.safe.rowMapper;

import cl.safe.to.Medico;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class MedicoMapper implements RowMapper<Medico>{

    public Medico mapRow(ResultSet rs, int rowNum) throws SQLException {
        Medico medico = new Medico();
        medico.setRut(rs.getInt("RUT_PERSONA"));
        medico.setNombre(rs.getString("NOMBRE_PERSONA"));
        medico.setApepat(rs.getString("APEPAT_PERSONA"));
        medico.setApemat(rs.getString("APEMAT_PERSONA"));
        medico.setEmail(rs.getString("EMAIL_PERSONA"));
        medico.setDireccion(rs.getString("DIRECCION_PERSONA"));
        medico.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
        medico.setCelular(rs.getString("CELULAR_PERSONA"));
        medico.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
        medico.setIdComuna(rs.getInt("ID_COMUNA"));
        medico.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        medico.setId(rs.getInt("ID_MEDICO"));
        medico.setEspecialidad("ESPECIALIDAD");
        
        return medico;
    }
}
