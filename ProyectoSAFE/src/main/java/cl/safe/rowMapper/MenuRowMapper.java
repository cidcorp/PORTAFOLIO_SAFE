package cl.safe.rowMapper;

import cl.safe.to.MenuTo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class MenuRowMapper implements RowMapper<MenuTo>{

    public MenuTo mapRow(ResultSet rs, int rowNum) throws SQLException {
        MenuTo menu = new MenuTo();
        menu.setId(rs.getInt("ID_MENU"));
        menu.setDescripcion(rs.getString("DESCRIPCION_MENU"));

        return menu;
    }
}
