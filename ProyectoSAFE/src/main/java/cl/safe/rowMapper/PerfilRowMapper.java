package cl.safe.rowMapper;

import cl.safe.to.Perfil;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PerfilRowMapper implements RowMapper<Perfil>{

    public Perfil mapRow(ResultSet rs, int rowNum) throws SQLException {
        Perfil perfil = new Perfil();
        perfil.setId(rs.getInt("ID_PERFIL"));
        perfil.setDescripcion(rs.getString("DESCRIPCION_PERFIL"));

        return perfil;
    }
}
