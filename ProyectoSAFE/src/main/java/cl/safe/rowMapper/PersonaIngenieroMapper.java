/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.safe.rowMapper;

import cl.safe.to.PersonaIngeniero;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author CidCorp
 */
public class PersonaIngenieroMapper implements RowMapper<PersonaIngeniero>{

    public PersonaIngeniero mapRow(ResultSet rs, int rowNum) throws SQLException {
        PersonaIngeniero pi = new PersonaIngeniero();
        pi.setRut(rs.getInt("RUT_PERSONA"));
        pi.setNombre(rs.getString("NOMBRE_PERSONA"));
        pi.setApepat(rs.getString("APEPAT_PERSONA"));
        pi.setApemat(rs.getString("APEMAT_PERSONA"));
        pi.setEmail(rs.getString("EMAIL_PERSONA"));
        pi.setDireccion(rs.getString("DIRECCION_PERSONA"));
        pi.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
        pi.setCelular(rs.getString("CELULAR_PERSONA"));
        pi.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
        pi.setIdComuna(rs.getInt("ID_COMUNA"));
        pi.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        pi.setIdIngeniero(rs.getInt("ID_INGENIERO"));
        
        return pi;
    }
}
