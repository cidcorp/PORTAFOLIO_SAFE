
package cl.safe.rowMapper;

import cl.safe.to.Persona;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;


public class PersonaMapper implements RowMapper<Persona>{

    public Persona mapRow(ResultSet rs, int rowNum) throws SQLException {
        Persona p = new Persona();
        p.setRut(rs.getInt("RUT_PERSONA"));
        p.setNombre(rs.getString("NOMBRE_PERSONA"));
        p.setApepat(rs.getString("APEPAT_PERSONA"));
        p.setApemat(rs.getString("APEMAT_PERSONA"));
        p.setEmail(rs.getString("EMAIL_PERSONA"));
        p.setDireccion(rs.getString("DIRECCION_PERSONA"));
        p.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
        p.setCelular(rs.getString("CELULAR_PERSONA"));
        p.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
        p.setIdComuna(rs.getInt("ID_COMUNA"));
        p.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        p.setEstado(rs.getString("ESTADO"));
        p.setTipoRol(rs.getInt("TIPO_ROL"));
        return p;
    }
}
