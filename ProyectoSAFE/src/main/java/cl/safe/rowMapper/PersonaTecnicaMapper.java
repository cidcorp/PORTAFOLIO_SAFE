package cl.safe.rowMapper;

import cl.safe.to.PersonaTecnico;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author CidCorp
 */
public class PersonaTecnicaMapper implements RowMapper<PersonaTecnico>{

    public PersonaTecnico mapRow(ResultSet rs, int rowNum) throws SQLException {
        PersonaTecnico pt = new PersonaTecnico();
        pt.setRut(rs.getInt("RUT_PERSONA"));
        pt.setNombre(rs.getString("NOMBRE_PERSONA"));
        pt.setApepat(rs.getString("APEPAT_PERSONA"));
        pt.setApemat(rs.getString("APEMAT_PERSONA"));
        pt.setEmail(rs.getString("EMAIL_PERSONA"));
        pt.setDireccion(rs.getString("DIRECCION_PERSONA"));
        pt.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
        pt.setCelular(rs.getString("CELULAR_PERSONA"));
        pt.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
        pt.setIdComuna(rs.getInt("ID_COMUNA"));
        pt.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        pt.setIdTecnico(rs.getInt("ID_TECNICO"));
        
        return pt;
    }
}
