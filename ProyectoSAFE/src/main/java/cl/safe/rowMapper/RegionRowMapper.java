
package cl.safe.rowMapper;

import cl.safe.to.Region;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class RegionRowMapper implements RowMapper<Region>{

    public Region mapRow(ResultSet rs, int rowNum) throws SQLException {
        Region region = new Region();
        region.setId(rs.getInt("ID_REGION"));
        region.setNombre(rs.getString("NOMBRE_REGION"));
        return region;
    }
}
