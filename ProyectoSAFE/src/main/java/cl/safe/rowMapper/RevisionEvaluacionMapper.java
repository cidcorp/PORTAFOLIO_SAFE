package cl.safe.rowMapper;

import cl.safe.to.RevisionEvaluacionEmpresa;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class RevisionEvaluacionMapper implements RowMapper<RevisionEvaluacionEmpresa>{

    public RevisionEvaluacionEmpresa mapRow(ResultSet rs, int rowNum) throws SQLException {
        RevisionEvaluacionEmpresa revEva = new RevisionEvaluacionEmpresa();
        revEva.setTipoEvaluacion(rs.getInt("TIPO_EVALUACION"));
        revEva.setId(rs.getInt("ID_EVALUACION"));
        revEva.setIdRevision(rs.getInt("ID_REVISION"));
        revEva.setIdIngeniero(rs.getInt("ID_INGENIERO"));
        revEva.setRecomendacion(rs.getString("RECOMENDACIONES"));
        revEva.setIdSupervisor(rs.getInt("ID_SUPERVISOR"));
        revEva.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        
        return revEva;
    }
}
