package cl.safe.rowMapper;

import cl.safe.to.Supervisor;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SupervisorRowMapper implements RowMapper<Supervisor>{

    public Supervisor mapRow(ResultSet rs, int rowNum) throws SQLException {
        Supervisor supervisor = new Supervisor();
        supervisor.setIdSupervisor(rs.getInt("ID_SUPERVISOR"));
        supervisor.setRut(rs.getInt("RUT_PERSONA"));
        supervisor.setNombre(rs.getString("NOMBRE_PERSONA"));
        supervisor.setApepat(rs.getString("APEPAT_PERSONA"));
        supervisor.setApemat(rs.getString("APEMAT_PERSONA"));
        supervisor.setEmail(rs.getString("EMAIL_PERSONA"));
        supervisor.setDireccion(rs.getString("DIRECCION_PERSONA"));
        supervisor.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
        supervisor.setCelular(rs.getString("CELULAR_PERSONA"));
        supervisor.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
        supervisor.setIdComuna(rs.getInt("ID_COMUNA"));
        supervisor.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        supervisor.setEstado(rs.getString("ESTADO"));
        return supervisor;
    }
}
