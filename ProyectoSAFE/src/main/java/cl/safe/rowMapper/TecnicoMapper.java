package cl.safe.rowMapper;

import cl.safe.to.Tecnico;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TecnicoMapper implements RowMapper<Tecnico>{

    public Tecnico mapRow(ResultSet rs, int rowNum) throws SQLException {
        Tecnico t = new Tecnico();
        t.setId(rs.getInt("ID_TECNICO"));
        t.setRut(rs.getInt("RUT_PERSONA"));
        return t;
    }
}
