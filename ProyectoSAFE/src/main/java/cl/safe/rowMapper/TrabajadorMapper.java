package cl.safe.rowMapper;

import cl.safe.to.Trabajador;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TrabajadorMapper implements RowMapper<Trabajador>{

    public Trabajador mapRow(ResultSet rs, int rowNum) throws SQLException {
        Trabajador t = new Trabajador();
        t.setRut(rs.getInt("RUT_PERSONA"));
        t.setCargo(rs.getString("CARGO_TRABAJADOR"));
        return t;
    }
}
