package cl.safe.rowMapper;

import cl.safe.to.Trabajador;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TrabajadorPersonaMapper implements RowMapper<Trabajador>{

    public Trabajador mapRow(ResultSet rs, int rowNum) throws SQLException {
        Trabajador t = new Trabajador();
        t.setRut(rs.getInt("RUT_PERSONA"));
        t.setNombre(rs.getString("NOMBRE_PERSONA"));
        t.setApepat(rs.getString("APEPAT_PERSONA"));
        t.setApemat(rs.getString("APEMAT_PERSONA"));
        t.setEmail(rs.getString("EMAIL_PERSONA"));
        t.setDireccion(rs.getString("DIRECCION_PERSONA"));
        t.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
        t.setCelular(rs.getString("CELULAR_PERSONA"));
        t.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
        t.setIdComuna(rs.getInt("ID_COMUNA"));
        t.setIdEmpresa(rs.getInt("ID_EMPRESA"));
        t.setEstado(rs.getString("ESTADO"));
        t.setId(rs.getInt("ID_TRABAJADOR"));
        t.setCargo(rs.getString("CARGO_TRABAJADOR"));
        
        return t;
    }
}
