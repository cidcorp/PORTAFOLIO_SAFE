package cl.safe.rowMapper;

import cl.safe.to.Empresa;
import cl.safe.to.Persona;
import cl.safe.to.Usuario;
import cl.safe.to.UsuarioAuth;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UsuarioRowMapper implements RowMapper<UsuarioAuth>{
    
    public UsuarioAuth mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsuarioAuth usuarioAuth = new UsuarioAuth();
        Empresa emp=null;
        Persona p=null;
        int tipo = rs.getInt("TIPO");
        
        Usuario usu  = new Usuario();
        usu.setId(rs.getInt("ID_USUARIO"));
        usu.setIdPerfil(rs.getInt("ID_PERFIL"));
        usu.setEstado(rs.getString("ESTADO"));
        
        if(tipo == 1){
            p = new Persona();
            p.setIdUsuario(rs.getInt("ID_USUARIO"));
            p.setEstado(rs.getString("ESTADO"));
            p.setRut(rs.getInt("RUT_PERSONA"));
            p.setNombre(rs.getString("NOMBRE_PERSONA"));
            p.setApepat(rs.getString("APEPAT_PERSONA"));
            p.setApemat(rs.getString("APEMAT_PERSONA"));
            p.setEmail(rs.getString("EMAIL_PERSONA"));
            p.setDireccion(rs.getString("DIRECCION_PERSONA"));
            p.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
            p.setCelular(rs.getString("CELULAR_PERSONA"));
            p.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
        }else{
            emp = new Empresa();
            emp.setEstado(rs.getString("ESTADO"));
            emp.setRut(rs.getInt("RUT_PERSONA"));
            emp.setNombre(rs.getString("NOMBRE_PERSONA"));
            emp.setEmail(rs.getString("EMAIL_PERSONA"));
            emp.setDireccion(rs.getString("DIRECCION_PERSONA"));
            emp.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
            emp.setTelefono(rs.getInt("TELEFONO"));
            emp.setRubro(rs.getString("RUBRO"));
        }
        usuarioAuth.setEmpresa(emp);
        usuarioAuth.setPersona(p);
        usuarioAuth.setUsuario(usu);
        
        return usuarioAuth ;
    }
}
