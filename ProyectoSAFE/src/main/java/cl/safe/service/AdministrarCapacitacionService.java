package cl.safe.service;

import cl.safe.dao.AdministrarCapacitacionDao;
import cl.safe.dao.impl.AdministrarCapacitacionDaoImpl;
import cl.safe.to.AsistenciaCapacitacion;
import cl.safe.to.Capacitacion;
import cl.safe.to.Expositor;
import cl.safe.to.Trabajador;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("capacitacion")
public class AdministrarCapacitacionService {

    @Autowired
    AdministrarCapacitacionDao admCapDaoApi = new AdministrarCapacitacionDaoImpl();
    
    public AdministrarCapacitacionService() { 
    }
  
    
    public List<Capacitacion> obtenerCapacitacionPorRutEmpresa(int rutEmpresa){
        return admCapDaoApi.obtenerCapacitacionPorRutEmpresa(rutEmpresa);
    }
            
    public List<Trabajador> obtenerTrabajadorPorEmpresa(int rutEmpresa){
        return   admCapDaoApi.obtenerTrabajadorPorEmpresa(rutEmpresa);
    }
    
//    public String modificarCapacitacion(Capacitacion cap){
//        return   admCapDaoApi.modificarCapacitacion(cap);
//    }
    public boolean eliminarAsistenciaMedica(int idAsistenciaCapacitacion){
        return  admCapDaoApi.eliminarAsistenciaMedica(idAsistenciaCapacitacion);
    }
    
    public List<Expositor> obtenerExpositores(){
        return  admCapDaoApi.obtenerExpositores();
    }

    public List<AsistenciaCapacitacion> obtenerAsistenciaCapacitacionPorId(int idCapacitacion){
        return  admCapDaoApi.obtenerAsistenciaCapacitacionPorId(idCapacitacion);
    }
//    public Boolean agregarCapacitacion(Capacitacion cap){
//        return   admCapDaoApi.agregarCapacitacion(cap);
//    }
    
    public Boolean MantenedorCapacitacion(Capacitacion cap){
        if(cap != null && cap.getId()== 0){
            return   admCapDaoApi.agregarCapacitacion(cap);
        }else if(cap != null && cap.getId() > 0) {
            return   admCapDaoApi.modificarCapacitacion(cap);
        }else{
            return null;
        }
    }
    
    public Boolean agregarAsistenciaCapacitacion(AsistenciaCapacitacion asistencia){
        return   admCapDaoApi.agregarAsistenciaCapacitacion(asistencia);
    }
}
