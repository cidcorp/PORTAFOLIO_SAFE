package cl.safe.service;

import cl.safe.dao.AdministrarEvaluacionesDao;
import cl.safe.dao.impl.AdministrarEvaluacionesDaoImpl;
import cl.safe.to.EmpresaComuna;
import cl.safe.to.Evaluacion;
import cl.safe.to.EvaluacionRevision;
import cl.safe.to.InstalacionComuna;
import cl.safe.to.Medico;
import cl.safe.to.PersonaIngeniero;
import cl.safe.to.PersonaTecnico;
import cl.safe.to.RevisionEvaluacionEmpresa;
import cl.safe.to.Tecnico;
import cl.safe.to.Trabajador;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("evalua")
public class AdministrarEvaluacionesService {

    @Autowired
    AdministrarEvaluacionesDao admEvaDaoApi = new AdministrarEvaluacionesDaoImpl();
    
    public AdministrarEvaluacionesService() { 
    }
  
    
    public List<Evaluacion> obtenerEvaluacionPorRut(int id){
        return   admEvaDaoApi.obtenerEvaluacionPorRut(id);
    }
            
    public Tecnico obtenerTecnico(int id){
        return   admEvaDaoApi.obtenerTecnico(id);
    }
    
    public Trabajador obtenerTrabajadorPorIdTencnico(int id){
        return   admEvaDaoApi.obtenerTrabajadorPorIdTencnico(id);
    }
    public List<PersonaTecnico> listarTecnicos(){
        return   admEvaDaoApi.listarTecnicos();
    }
    
    public List<PersonaIngeniero> listarIngenieros(){
        return   admEvaDaoApi.listarIngenieros();
    }
    
    public List<InstalacionComuna> listarInstalacionEmpresas(int rutEmpresa){
        return   admEvaDaoApi.listarInstalacionEmpresas(rutEmpresa);
    }
    
    public List<EmpresaComuna> listarEmpresasComuna(){
        return   admEvaDaoApi.listarEmpresasComuna();
    }
    
    public Boolean addAndUpdateEvaluacion(Evaluacion eva){
        if(eva != null && eva.getId() == 0 ){
            if(eva.getTipoEvaluacion()== 1){
                return   admEvaDaoApi.agregarEvaluacionInstalacion(eva);
            }else{
                return   admEvaDaoApi.agregarEvaluacionTrabajador(eva);
            }
        }else if(eva != null && eva.getId() > 0) { 
            if(eva.getTipoEvaluacion() == 1){
                return admEvaDaoApi.modificarEvaluacionInstalacion(eva);
            }
             else{
                return admEvaDaoApi.modificarEvaluacionTrabajador(eva);
            }
        }else{
            return null;
        }
            
    }
    
    public Boolean addAndUpdateRevEvaluacion(EvaluacionRevision eva){
        
        if(eva != null && eva.getId() == 0 ){
            if(eva.getTipoRevEvaluacion()== 1){
                return   admEvaDaoApi.agregarEvaluacionRevInstalacion(eva);
            }else{
                return   admEvaDaoApi.agregarEvaluacionRevTrabajador(eva);
            }
        }else if(eva != null && eva.getId() > 0) { 
            if(eva.getTipoRevEvaluacion()== 1){
            return admEvaDaoApi.modificarEvaluacionRevInstalacion(eva);
            }
             else{
                return admEvaDaoApi.modificarEvaluacionRevTrabajador(eva);
            }
        }else{
            return null;
        }
    }
    
    public List<RevisionEvaluacionEmpresa> obtenerRevisionEvaluacionesPorIdEmpresa(int idEvaluacion){
        return   admEvaDaoApi.obtenerRevisionEvaluacionesPorIdEmpresa(idEvaluacion);
    }
    
}
