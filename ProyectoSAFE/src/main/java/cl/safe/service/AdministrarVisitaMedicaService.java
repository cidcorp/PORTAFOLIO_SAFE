package cl.safe.service;

import cl.safe.dao.AdministrarVisitaMedicaDao;
import cl.safe.dao.impl.AdministrarVisitaMedicaDaoImpl;
import cl.safe.to.AgendaMedica;
import cl.safe.to.AtencionMedica;
import cl.safe.to.ExamenEmpresa;
import cl.safe.to.ExamenFichaMedica;
import cl.safe.to.FichaMedica;
import cl.safe.to.Medico;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("visitaMedica")
public class AdministrarVisitaMedicaService {

    @Autowired
    AdministrarVisitaMedicaDao visitaMedDaoApi = new AdministrarVisitaMedicaDaoImpl();
    
    public AdministrarVisitaMedicaService() { 
    }

    public List<AgendaMedica> obtenerAgendaMedicaPorRutEmpresa(int rutEmpresa){
        return visitaMedDaoApi.obtenerAgendaMedicaPorRutEmpresa(rutEmpresa);
    }
    
    public List<ExamenEmpresa> obtenerExamenesEmpresa(int rutEmpresa){
        return visitaMedDaoApi.obtenerExamenesEmpresa(rutEmpresa);
    }
    
    public List<FichaMedica> obtenerFichaMedica(int rutEmpresa){
        return visitaMedDaoApi.obtenerFichaMedica(rutEmpresa);
    }
    public List<Medico> obtenerMedicos(){
        return visitaMedDaoApi.obtenerMedicos();
    }
    public AgendaMedica obtenerAgendaMedicaPorId(int idAgendaMedica){
        return visitaMedDaoApi.obtenerAgendaMedicaPorId(idAgendaMedica);
    }
    public Boolean mantenedorAgendaMedica(AgendaMedica agenda){
        if(agenda != null && agenda.getId() == 0){
            return   visitaMedDaoApi.agregarAgendaMedica(agenda);
        }else if(agenda != null && agenda.getId() > 0) {
            return   visitaMedDaoApi.modificarAgendaMedica(agenda);
        }else{
            return null;
        }
        
    }
   
    public Boolean mantenedorAtencionMedica(AtencionMedica atencionMedica){
        
        if(atencionMedica != null && atencionMedica.getId() == 0){
            return   visitaMedDaoApi.agregarAtencionMedica(atencionMedica);
        }else if(atencionMedica != null && atencionMedica.getId() > 0) {
            return   visitaMedDaoApi.modificarAtencionMedica(atencionMedica);
        }else{
            return null;
        }
    }
    
    public Boolean mantenedorFichaMedica(FichaMedica fichaMedica){
        if(fichaMedica != null && fichaMedica.getId() == 0){
            return   visitaMedDaoApi.agregarFichaMedica(fichaMedica);
        }else if(fichaMedica != null && fichaMedica.getId() > 0) {
            return   visitaMedDaoApi.modificarFichaMedica(fichaMedica);
        }else{
            return null;
        }
    }
 
    public Boolean agregarExamenFichaMedica(ExamenFichaMedica examenFichaMedica){
        return visitaMedDaoApi.agregarExamenFichaMedica(examenFichaMedica);
    }
    public Boolean eliminarExamenFichaMedica(int id){
            return visitaMedDaoApi.eliminarExamenFichaMedica(id);
    }
    
    public List<AtencionMedica> obtenerAtencionMedicaPorIdAgendaMedica(int idEmpresa){
        return visitaMedDaoApi.obtenerAtencionMedicaPorIdAgendaMedica(idEmpresa);
    }
}
