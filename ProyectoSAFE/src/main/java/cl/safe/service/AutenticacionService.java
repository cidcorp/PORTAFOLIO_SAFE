package cl.safe.service;

import cl.safe.dao.AutenticarDao;
import cl.safe.dao.impl.AutenticarDaoImpl;
import cl.safe.to.Login;
import cl.safe.to.TokenTo;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.time.ZonedDateTime;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("aut")
public class AutenticacionService {
    
    @Autowired
    AutenticarDao autDao = new AutenticarDaoImpl();

    static final String JWT_TOKEN_KEY = "safe_XXASEZ"; 
    
    public AutenticacionService() {
    }
    
    public TokenTo autenticarUsuario(Login login){
        TokenTo token = new TokenTo();
        Login pass =  autDao.autenticarUsuario(login);
        if(pass !=null){
            if( login.getPassword().equals(pass.getPassword()) ){
              Date fechaExpiracion = Date.from(ZonedDateTime.now().plusHours(1).toInstant());
                System.out.println("fechaExpiracion " + fechaExpiracion);
              token = this.crearToken(login.getRutUsuario(), fechaExpiracion);

            }else{
                token.setLoginCorrecto(false);
            }
        }else{
            token =null;
        }   
       return token; 
    }
    
    public TokenTo crearToken(int rutUsuario, Date fechaExpiracion){
        TokenTo tokenTo = new TokenTo();
        String tokenGenerado="";
        try {
        Algorithm algorithm = Algorithm.HMAC256(JWT_TOKEN_KEY);
        Date issuedAt = Date.from(ZonedDateTime.now().toInstant());
        tokenGenerado =  JWT.create()
                .withIssuedAt(issuedAt)
                .withExpiresAt(fechaExpiracion)
                .withClaim("rutUsuario", rutUsuario)
                .withIssuer("safe_auth")
                .sign(algorithm);
           
        } catch (JWTCreationException exception){
            tokenTo = null;
        }
        tokenTo.setLoginCorrecto(true);
        tokenTo.setToken(tokenGenerado);
        tokenTo.setFechaExpiracion(fechaExpiracion);
                
        return tokenTo;
    }
    
     public Boolean validarToken(String token){
        Boolean resp =true;
        try {
            String[] valor = token.split(" ");
        if(valor !=null && valor.length >1){
             if(valor[0].equalsIgnoreCase("Bearer") && valor.length == 2){
             JWTVerifier verifier = JWT.require(Algorithm.HMAC256(JWT_TOKEN_KEY))
                        .withIssuer("safe_auth")
                        .build();
                DecodedJWT jwt = verifier.verify(valor[1]);
                 jwt.getClaim("rutUsuario").asString();   
            }
              else {
                resp = false;
            }
        }
         else{
            resp = false;
        }
            
    } catch (JWTVerificationException e){
            resp = false;
            System.out.println("Error " + e.getMessage());
    }
        return resp;
         
    }
}
