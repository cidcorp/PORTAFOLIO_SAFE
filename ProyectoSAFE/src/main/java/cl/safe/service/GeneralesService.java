package cl.safe.service;

import cl.safe.dao.GeneralesDao;
import cl.safe.dao.impl.GeneralesDaoImpl;
import cl.safe.to.Comuna;
import cl.safe.to.Region;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("generalesService")
public class GeneralesService {
    
    @Autowired
    GeneralesDao generalesDao = new GeneralesDaoImpl() ;
    
    
    public List<Region> obtenerRegiones(){        
        return generalesDao.obtenerRegiones();
    }
    public List<Comuna> obtenerComunaPorRegion(int idRegion){
        return generalesDao.obtenerComunaPorRegion(idRegion);
    }
    public Boolean onBd(){
        return generalesDao.onBd();
    }
    
}
