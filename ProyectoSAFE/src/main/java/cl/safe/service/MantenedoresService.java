package cl.safe.service;

import cl.safe.dao.impl.MantenedoresDaoImpl;
import cl.safe.to.Empresa;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cl.safe.dao.MantenedoresDao;
import cl.safe.to.Administrador;
import cl.safe.to.AsociacionPerfilMenu;
import cl.safe.to.Instalacion;
import cl.safe.to.Medico;
import cl.safe.to.MenuTo;
import cl.safe.to.Perfil;
import cl.safe.to.Persona;
import cl.safe.to.Supervisor;
import cl.safe.to.Trabajador;
import cl.safe.to.Usuario;
import cl.safe.to.UsuarioAuth;

@Service("mantenedores")
public class MantenedoresService {

    @Autowired
    MantenedoresDao mantenedoresDaoImpl = new MantenedoresDaoImpl();
    
    public MantenedoresService() { 
    }
     
    public Boolean mantenedorEmpresa(Empresa emp){
        if(emp != null && emp.getId() == 0){
            return   mantenedoresDaoImpl.agregarEmpresa(emp);
        }else if(emp != null && emp.getId() > 0) {
            return   mantenedoresDaoImpl.modificarEmpresa(emp);
        }else{
            return null;
        }
    }
    
    public Boolean mantenedorUsuario(Usuario usu){
        if(usu != null && usu.getId()== 0){
            return   mantenedoresDaoImpl.agregarUsuario(usu);
        }else if(usu != null && usu.getId() > 0) {
            return   mantenedoresDaoImpl.modificarUsuario(usu);
        }else{
            return null;
        }
    }
    
    public Boolean mantenedorPersona(Persona persona){
        if(persona != null && persona.getAccion() == 1){
            return   mantenedoresDaoImpl.agregarPersona(persona);
        }else {
            return   mantenedoresDaoImpl.modificarPersona(persona);
        }
    }
    
       public Boolean mantenedorMedico(Medico medico){
           
        if(medico != null && medico.getRut()== 0){
            return   mantenedoresDaoImpl.agregarMedico(medico);
        }else if(medico != null && medico.getRut() > 0) {
            return   mantenedoresDaoImpl.modificarMedico(medico);
        }else{
            return null;
        }
    }
    
    public Boolean mantenedorTrabajador(Trabajador trabajador){
        
        if(trabajador != null && trabajador.getId()== 0){
            return   mantenedoresDaoImpl.agregarTrabajador(trabajador);
        }else if(trabajador != null && trabajador.getId() > 0) {
            return   mantenedoresDaoImpl.modificarTrabajador(trabajador);
        }else{
            return null;
        }
    }
    
    public Boolean mantenedorInstalacion(Instalacion instalacion){
        
        if(instalacion != null && instalacion.getId()== 0){
            return   mantenedoresDaoImpl.agregarInstalacion(instalacion);
        }else if(instalacion != null && instalacion.getId() > 0) {
            return   mantenedoresDaoImpl.modificarInstalacion(instalacion);
        }else{
            return null;
        }
    }
    
    public List<Supervisor> obtenerSupervisores(){
        return mantenedoresDaoImpl.obtenerSupervisores();
    }
    
    public List<Perfil> obtenerPerfiles(){
        return mantenedoresDaoImpl.obtenerPerfiles();
    }
    
    public List<MenuTo> obtenerMenu(){
        return mantenedoresDaoImpl.obtenerMenu();
    }
    
    public List<UsuarioAuth> obtenerUsuario(){
        return mantenedoresDaoImpl.obtenerUsuario();
    }
    
    public Persona obtenerPersona(int rutPersona){
        return mantenedoresDaoImpl.obtenerPersona(rutPersona);
    }
    
    public List<Administrador> obtenerAdministrador(){
        return mantenedoresDaoImpl.obtenerAdministrador();
    }
    
    public List<AsociacionPerfilMenu> obtenerAsociacion_perfil_menu(){
        return mantenedoresDaoImpl.obtenerAsociacion_perfil_menu();
    }
    
    
}
