
package cl.safe.to;

public class Administrador extends Persona{
    
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Administrador{" + "id=" + id + '}';
    }
            
}
