
package cl.safe.to;

import java.util.Date;

public class AgendaMedica {
    
    private int id;
    private Date fechaDisponible;
    private int idMedico;
    private String confirmado;
    private int idEmpresa;
    private int accion;

    public AgendaMedica() {
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Date getFechaDisponible() {
        return fechaDisponible;
    }
    public void setFechaDisponible(Date fechaDisponible) {
        this.fechaDisponible = fechaDisponible;
    }
    public int getIdMedico() {
        return idMedico;
    }
    public void setIdMedico(int idMedico) {
        this.idMedico = idMedico;
    }
    public String getConfirmado() {
        return confirmado;
    }
    public void setConfirmado(String confirmado) {
        this.confirmado = confirmado;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }
    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getAccion() {
        return accion;
    }

    public void setAccion(int accion) {
        this.accion = accion;
    }

    @Override
    public String toString() {
        return "AgendaMedica{" + "id=" + id + ", fechaDisponible=" + fechaDisponible + ", idMedico=" + idMedico + ", confirmado=" + confirmado + ", idEmpresa=" + idEmpresa + ", accion=" + accion + '}';
    }

   
    
}
