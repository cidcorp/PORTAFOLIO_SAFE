
package cl.safe.to;

import java.util.Date;

public class AsistenciaCapacitacion {
    
    private int id;
    private Date fecha;
    private int registro;
    private int idTrabajador;

    public AsistenciaCapacitacion() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Date getFecha() {
        return fecha;
    }
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public int getRegistro() {
        return registro;
    }
    public void setRegistro(int registro) {
        this.registro = registro;
    }
    public int getIdTrabajador() {
        return idTrabajador;
    }
    public void setIdTrabajador(int idTrabajador) {
        this.idTrabajador = idTrabajador;
    }
    @Override
    public String toString() {
        return "AsistenciaCapacitacion{" + "id=" + id + ", fecha=" + fecha + ", registro=" + registro + ", idTrabajador=" + idTrabajador + '}';
    }    
}
