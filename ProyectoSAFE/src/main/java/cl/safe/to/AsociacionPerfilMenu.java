package cl.safe.to;

public class AsociacionPerfilMenu {
    private int idPerfil;
    private int idMenu;

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    @Override
    public String toString() {
        return "AsociacionPerfilMenu{" + "idPerfil=" + idPerfil + ", idMenu=" + idMenu + '}';
    }
    
    
}
