package cl.safe.to;

import java.util.Date;

public class AtencionMedica {
    
    private int id;
    private Date fechaAtencion;
    private String recomendacionesAM;
    private int idAgendaMedico;
    private int idEmpresa;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaAtencion() {
        return fechaAtencion;
    }

    public void setFechaAtencion(Date fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    public String getRecomendacionesAM() {
        return recomendacionesAM;
    }

    public void setRecomendacionesAM(String recomendacionesAM) {
        this.recomendacionesAM = recomendacionesAM;
    }

    public int getIdAgendaMedico() {
        return idAgendaMedico;
    }

    public void setIdAgendaMedico(int idAgendaMedico) {
        this.idAgendaMedico = idAgendaMedico;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public String toString() {
        return "AtencionMedica{" + "id=" + id + ", fechaAtencion=" + fechaAtencion + ", recomendacionesAM=" + recomendacionesAM + ", idAgendaMedico=" + idAgendaMedico + ", idEmpresa=" + idEmpresa + '}';
    }
    
}
