package cl.safe.to;

import java.util.Date;

public class Capacitacion {
    
    private int id;
    private String asunto;
    private Date fechaInicio;
    private Date fechaTermino;
    private int cupoMinimo;
    private int idSupervisor;
    private int idExpositor;
    private int idEmpresa;
    private int cantidadAsistencia;

    public Capacitacion() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public int getCupoMinimo() {
        return cupoMinimo;
    }

    public void setCupoMinimo(int cupoMinimo) {
        this.cupoMinimo = cupoMinimo;
    }

    public int getIdSupervisor() {
        return idSupervisor;
    }

    public void setIdSupervisor(int idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    public int getIdExpositor() {
        return idExpositor;
    }

    public void setIdExpositor(int idExpositor) {
        this.idExpositor = idExpositor;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getCantidadAsistencia() {
        return cantidadAsistencia;
    }

    public void setCantidadAsistencia(int cantidadAsistencia) {
        this.cantidadAsistencia = cantidadAsistencia;
    }

    @Override
    public String toString() {
        return "Capacitacion{" + "id=" + id + ", asunto=" + asunto + ", fechaInicio=" + fechaInicio + ", fechaTermino=" + fechaTermino + ", cupoMinimo=" + cupoMinimo + ", idSupervisor=" + idSupervisor + ", idExpositor=" + idExpositor + ", idEmpresa=" + idEmpresa + ", cantidadAsistencia=" + cantidadAsistencia + '}';
    }

}
