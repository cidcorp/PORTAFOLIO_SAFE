package cl.safe.to;

public class Comuna {
    private int id;
    private String nombre;
    private Region region;
    
    public Comuna() {
        this.region = new Region();
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }    
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "Comuna{" + "id=" + id + ", nombre=" + nombre + ", region=" + region + '}';
    }
    
}
