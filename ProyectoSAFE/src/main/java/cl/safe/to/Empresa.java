
package cl.safe.to;

import java.util.Date;

public class Empresa {
    
    private int id;
    private int rut;
    private String nombre;
    private int telefono;
    private String direccion;
    private Date fechaIngreso;
    private String rubro;
    private String estado;
    private int idComuna;
    private Date fechaTermino;
    private int accion;
    private int idUsuario;
    private String email;

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public int getAccion() {
        return accion;
    }

    public void setAccion(int accion) {
        this.accion = accion;
    }

    public int getIdComuna() {
        return idComuna;
    }

    public void setIdComuna(int idComuna) {
        this.idComuna = idComuna;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Empresa{" + "id=" + id + ", rut=" + rut + ", nombre=" + nombre + ", telefono=" + telefono + ", direccion=" + direccion + ", fechaIngreso=" + fechaIngreso + ", rubro=" + rubro + ", estado=" + estado + ", idComuna=" + idComuna + ", fechaTermino=" + fechaTermino + ", accion=" + accion + ", idUsuario=" + idUsuario + ", email=" + email + '}';
    }
    
}
