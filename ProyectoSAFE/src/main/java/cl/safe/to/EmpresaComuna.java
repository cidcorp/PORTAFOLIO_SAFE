package cl.safe.to;

import java.util.Date;

public class EmpresaComuna {

    private Empresa empresa;
    private Comuna comuna;
    private Region region;
    

    public EmpresaComuna() {
        this.empresa = new Empresa();
        this.comuna = new Comuna();
        this.region = new Region();
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Comuna getComuna() {
        return comuna;
    }

    public void setComuna(Comuna comuna) {
        this.comuna = comuna;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    
}
