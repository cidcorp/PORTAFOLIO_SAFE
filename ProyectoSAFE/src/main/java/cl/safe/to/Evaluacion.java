package cl.safe.to;

import java.util.Date;

public class Evaluacion{
        
    private int id;
    private Date fechaEvaluacion;
    private String observacion;
    private int idTecnico;
    private int idInstaoTraba;
    private int informeOk;
    private int etapaEva;
    private int tipoEvaluacion;
    private int idComuna;
    private int idRegion;
    private int idEmpresa;

    public Evaluacion() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaEvaluacion() {
        return fechaEvaluacion;
    }

    public void setFechaEvaluacion(Date fechaEvaluacion) {
        this.fechaEvaluacion = fechaEvaluacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getIdTecnico() {
        return idTecnico;
    }

    public void setIdTecnico(int idTecnico) {
        this.idTecnico = idTecnico;
    }
    public int getInformeOk() {
        return informeOk;
    }

    public void setInformeOk(int informeOk) {
        this.informeOk = informeOk;
    }

    public int getTipoEvaluacion() {
        return tipoEvaluacion;
    }

    public void setTipoEvaluacion(int tipoEvaluacion) {
        this.tipoEvaluacion = tipoEvaluacion;
    }

    public int getIdInstaoTraba() {
        return idInstaoTraba;
    }
    public void setIdInstaoTraba(int idInstaoTraba) {
        this.idInstaoTraba = idInstaoTraba;
    }
    public int getIdComuna() {
        return idComuna;
    }
    public void setIdComuna(int idComuna) {
        this.idComuna = idComuna;
    }
    public int getIdRegion() {
        return idRegion;
    }
    public void setIdRegion(int idRegion) {
        this.idRegion = idRegion;
    }

    public int getEtapaEva() {
        return etapaEva;
    }

    public void setEtapaEva(int etapaEva) {
        this.etapaEva = etapaEva;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public String toString() {
        return "Evaluacion{" + "id=" + id + ", fechaEvaluacion=" + fechaEvaluacion + ", observacion=" + observacion + ", idTecnico=" + idTecnico + ", idInstaoTraba=" + idInstaoTraba + ", informeOk=" + informeOk + ", etapaEva=" + etapaEva + ", tipoEvaluacion=" + tipoEvaluacion + ", idComuna=" + idComuna + ", idRegion=" + idRegion + ", idEmpresa=" + idEmpresa + '}';
    }
    
}
