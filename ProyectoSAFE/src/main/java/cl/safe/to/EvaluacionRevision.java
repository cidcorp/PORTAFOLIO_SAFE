package cl.safe.to;

public class EvaluacionRevision {

    private int id;
    private int idIngeniero;
    private int idEvaInstOTrab;
    private String recomendaciones;
    private int idSupervisor;
    private int tipoRevEvaluacion;

    public EvaluacionRevision() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdIngeniero() {
        return idIngeniero;
    }

    public void setIdIngeniero(int idIngeniero) {
        this.idIngeniero = idIngeniero;
    }

    public int getIdEvaInstOTrab() {
        return idEvaInstOTrab;
    }

    public void setIdEvaInstOTrab(int idEvaInstOTrab) {
        this.idEvaInstOTrab = idEvaInstOTrab;
    }

    public String getRecomendaciones() {
        return recomendaciones;
    }

    public void setRecomendaciones(String recomendaciones) {
        this.recomendaciones = recomendaciones;
    }

    public int getIdSupervisor() {
        return idSupervisor;
    }

    public void setIdSupervisor(int idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    public int getTipoRevEvaluacion() {
        return tipoRevEvaluacion;
    }
    public void setTipoRevEvaluacion(int tipoRevEvaluacion) {
        this.tipoRevEvaluacion = tipoRevEvaluacion;
    }

    @Override
    public String toString() {
        return "EvaluacionRevision{" + "id=" + id + ", idIngeniero=" + idIngeniero + ", idEvaInstOTrab=" + idEvaInstOTrab + ", recomendaciones=" + recomendaciones + ", idSupervisor=" + idSupervisor + ", tipoRevEvaluacion=" + tipoRevEvaluacion + '}';
    }
    
}
