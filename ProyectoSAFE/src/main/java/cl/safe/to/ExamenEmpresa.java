package cl.safe.to;

import java.util.Date;

public class ExamenEmpresa {
    
    private int id;
    private String tipoExamen;
    private Date FechaExamen;
    private int idFichaMedica ;
    private int idEmpresa;
    
    public ExamenEmpresa() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipoExamen() {
        return tipoExamen;
    }

    public void setTipoExamen(String tipoExamen) {
        this.tipoExamen = tipoExamen;
    }

    public Date getFechaExamen() {
        return FechaExamen;
    }

    public void setFechaExamen(Date FechaExamen) {
        this.FechaExamen = FechaExamen;
    }

    public int getIdFichaMedica() {
        return idFichaMedica;
    }

    public void setIdFichaMedica(int idFichaMedica) {
        this.idFichaMedica = idFichaMedica;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public String toString() {
        return "Examen{" + "id=" + id + ", tipoExamen=" + tipoExamen + ", FechaExamen=" + FechaExamen + ", idFichaMedica=" + idFichaMedica + ", idEmpresa=" + idEmpresa + '}';
    }
}
