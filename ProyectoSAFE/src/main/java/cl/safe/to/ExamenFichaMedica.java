
package cl.safe.to;

import java.util.Date;

public class ExamenFichaMedica {
   private int id;
   private String tipoExamen;
   private Date fechaExamen;
   private int idFichaMedica;
   private int accion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipoExamen() {
        return tipoExamen;
    }

    public void setTipoExamen(String tipoExamen) {
        this.tipoExamen = tipoExamen;
    }

    public Date getFechaExamen() {
        return fechaExamen;
    }

    public void setFechaExamen(Date fechaExamen) {
        this.fechaExamen = fechaExamen;
    }

    public int getIdFichaMedica() {
        return idFichaMedica;
    }

    public void setIdFichaMedica(int idFichaMedica) {
        this.idFichaMedica = idFichaMedica;
    }

    public int getAccion() {
        return accion;
    }

    public void setAccion(int accion) {
        this.accion = accion;
    }

    @Override
    public String toString() {
        return "ExamenFichaMedica{" + "id=" + id + ", tipoExamen=" + tipoExamen + ", fechaExamen=" + fechaExamen + ", idFichaMedica=" + idFichaMedica + ", accion=" + accion + '}';
    }
   
   
}
