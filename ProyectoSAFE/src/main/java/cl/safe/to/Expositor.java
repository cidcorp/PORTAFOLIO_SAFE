package cl.safe.to;

public class Expositor extends Persona{
    
    private int id;
    private int rutPersona;

    public Expositor() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getRutPersona() {
        return rutPersona;
    }
    public void setRutPersona(int rutPersona) {
        this.rutPersona = rutPersona;
    }
    @Override
    public String toString() {
        return "Expositor{" + "id=" + id + ", rutPersona=" + rutPersona + '}';
    }
    
}
