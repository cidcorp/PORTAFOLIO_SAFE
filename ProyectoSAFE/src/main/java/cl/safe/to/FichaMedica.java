package cl.safe.to;

public class FichaMedica {
    private int id;
    private String diagnostico;
    private int idTrabajador;
    private int idAtencionMedica;
    private int idEmpresa;
    
    public FichaMedica() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String Diagnostico) {
        this.diagnostico = Diagnostico;
    }

    public int getIdTrabajador() {
        return idTrabajador;
    }

    public void setIdTrabajador(int idTrabajador) {
        this.idTrabajador = idTrabajador;
    }

    public int getIdAtencionMedica() {
        return idAtencionMedica;
    }

    public void setIdAtencionMedica(int idAtencionMedica) {
        this.idAtencionMedica = idAtencionMedica;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public String toString() {
        return "FichaMedica{" + "id=" + id + ", diagnostico=" + diagnostico + ", idTrabajador=" + idTrabajador + ", idAtencionMedica=" + idAtencionMedica + ", idEmpresa=" + idEmpresa + '}';
    }

    
    
}
