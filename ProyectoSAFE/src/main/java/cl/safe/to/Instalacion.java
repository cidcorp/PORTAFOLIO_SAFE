package cl.safe.to;

public class Instalacion {
    private int id;
    private int telefono;
    private String direccion;
    private int idEmpresa;
    
    public Instalacion() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public String toString() {
        return "Instalacion{" + "id=" + id + ", telefono=" + telefono + ", direccion=" + direccion + ", idEmpresa=" + idEmpresa + '}';
    }

}
