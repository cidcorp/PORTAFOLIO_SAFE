package cl.safe.to;

public class InstalacionComuna {
    private int idInstalacion;
    private int telefonoInstalacion;
    private String direccionIntalacion;
    private int idEmpresa;
    private int idComuna;
    private String nombreComuna;
    private int idRegion;

    public InstalacionComuna() {
    }

    public int getIdInstalacion() {
        return idInstalacion;
    }

    public void setIdInstalacion(int idInstalacion) {
        this.idInstalacion = idInstalacion;
    }

    public int getTelefonoInstalacion() {
        return telefonoInstalacion;
    }

    public void setTelefonoInstalacion(int telefonoInstalacion) {
        this.telefonoInstalacion = telefonoInstalacion;
    }

    public String getDireccionIntalacion() {
        return direccionIntalacion;
    }

    public void setDireccionIntalacion(String direccionIntalacion) {
        this.direccionIntalacion = direccionIntalacion;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getIdComuna() {
        return idComuna;
    }

    public void setIdComuna(int idComuna) {
        this.idComuna = idComuna;
    }

    public String getNombreComuna() {
        return nombreComuna;
    }

    public void setNombreComuna(String nombreComuna) {
        this.nombreComuna = nombreComuna;
    }

    public int getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(int idRegion) {
        this.idRegion = idRegion;
    }

    @Override
    public String toString() {
        return "InstalacionComuna{" + "idInstalacion=" + idInstalacion + ", telefonoInstalacion=" + telefonoInstalacion + ", direccionIntalacion=" + direccionIntalacion + ", idEmpresa=" + idEmpresa + ", idComuna=" + idComuna + ", nombreComuna=" + nombreComuna + ", idRegion=" + idRegion + '}';
    }
    
}
