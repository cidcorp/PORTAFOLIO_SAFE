
package cl.safe.to;


public class Login {
    
    private int rutUsuario;
    private String password;
    
    public Login() {
    }

    public int getRutUsuario() {
        return rutUsuario;
    }

    public void setRutUsuario(int rutUsuario) {
        this.rutUsuario = rutUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Login{" + "rutUsuario=" + rutUsuario + ", password=" + password + '}';
    }

    
     
}
