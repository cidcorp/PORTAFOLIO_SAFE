package cl.safe.to;

public class Medico extends Persona{
    
    private long id;
    private String especialidad;

    public Medico() {
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getEspecialidad() {
        return especialidad;
    }
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    @Override
    public String toString() {
        return "Medico{" + "id=" + id + ", especialidad=" + especialidad + '}';
    }
    
}
