
package cl.safe.to;


public class MenuTo {
    private int id;
    private String descripcion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "MenuTo{" + "id=" + id + ", descripcion=" + descripcion + '}';
    }
    
    
}
