package cl.safe.to;


public class Perfil {

    private int id;
    private String descripcion;

    public Perfil() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Perfil{" + "id=" + id + ", descripcion=" + descripcion + '}';
    }
    
}
