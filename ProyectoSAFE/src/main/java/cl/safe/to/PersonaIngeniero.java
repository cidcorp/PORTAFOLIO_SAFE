
package cl.safe.to;

import java.util.Date;

public class PersonaIngeniero {
    
    private int rut;
    private String nombre;
    private String apepat;
    private String apemat;
    private String email;
    private String direccion;
    private Date fechaIngreso;
    private String celular;
    private Date fechaNacimiento;
    private int idComuna;
    private int idEmpresa;
    private int idIngeniero;
    
    public PersonaIngeniero() {
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApepat() {
        return apepat;
    }

    public void setApepat(String apepat) {
        this.apepat = apepat;
    }

    public String getApemat() {
        return apemat;
    }

    public void setApemat(String apemat) {
        this.apemat = apemat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getIdComuna() {
        return idComuna;
    }

    public void setIdComuna(int idComuna) {
        this.idComuna = idComuna;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getIdIngeniero() {
        return idIngeniero;
    }

    public void setIdIngeniero(int idIngeniero) {
        this.idIngeniero = idIngeniero;
    }
    @Override
    public String toString() {
        return "PersonaIngeniero{" + "rut=" + rut + ", nombre=" + nombre + ", apepat=" + apepat + ", apemat=" + apemat + ", email=" + email + ", direccion=" + direccion + ", fechaIngreso=" + fechaIngreso + ", celular=" + celular + ", fechaNacimiento=" + fechaNacimiento + ", idComuna=" + idComuna + ", idEmpresa=" + idEmpresa + ", idIngeniero=" + idIngeniero + '}';
    }

}
