package cl.safe.to;

import java.util.Date;

public class RevisionEvaluacion{
        
    private int id;
    private Date fechaEvaluacion;
    private String observacion;
    private int idTecnico;
    private int idInstalacion;
    private int informeOk;
    private int etapaEva;
    private int tipoEvaluacion;

    public RevisionEvaluacion() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFechaEvaluacion() {
        return fechaEvaluacion;
    }

    public void setFechaEvaluacion(Date fechaEvaluacion) {
        this.fechaEvaluacion = fechaEvaluacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getIdTecnico() {
        return idTecnico;
    }

    public void setIdTecnico(int idTecnico) {
        this.idTecnico = idTecnico;
    }

    public int getIdInstalacion() {
        return idInstalacion;
    }

    public void setIdInstalacion(int idInstalacion) {
        this.idInstalacion = idInstalacion;
    }

    public int getInformeOk() {
        return informeOk;
    }

    public void setInformeOk(int informeOk) {
        this.informeOk = informeOk;
    }

    public int getEtapaEva() {
        return etapaEva;
    }

    public void setEtapaEva(int etapaEva) {
        this.etapaEva = etapaEva;
    }

    public int getTipoEvaluacion() {
        return tipoEvaluacion;
    }

    public void setTipoEvaluacion(int tipoEvaluacion) {
        this.tipoEvaluacion = tipoEvaluacion;
    }

    @Override
    public String toString() {
        return "Evaluacion{" + "id=" + id + ", fechaEvaluacion=" + fechaEvaluacion + ", observacion=" + observacion + ", idTecnico=" + idTecnico + ", idInstalacion=" + idInstalacion + ", informeOk=" + informeOk + ", etapaEva=" + etapaEva + ", tipoEvaluacion=" + tipoEvaluacion + '}';
    }
    
}
