package cl.safe.to;

import java.util.Date;

public class RevisionEvaluacionEmpresa{
        
    private int tipoEvaluacion;
    private int id;
    private int idRevision;
    private int idIngeniero;       
    private String recomendacion;
    private int idSupervisor;
    private int idEmpresa;

    public RevisionEvaluacionEmpresa() {
    }

    public int getTipoEvaluacion() {
        return tipoEvaluacion;
    }

    public void setTipoEvaluacion(int tipoEvaluacion) {
        this.tipoEvaluacion = tipoEvaluacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdRevision() {
        return idRevision;
    }

    public void setIdRevision(int idRevision) {
        this.idRevision = idRevision;
    }

    public int getIdIngeniero() {
        return idIngeniero;
    }

    public void setIdIngeniero(int idIngeniero) {
        this.idIngeniero = idIngeniero;
    }
    
    public String getRecomendacion() {
        return recomendacion;
    }

    public void setRecomendacion(String recomendacion) {
        this.recomendacion = recomendacion;
    }

    public int getIdSupervisor() {
        return idSupervisor;
    }

    public void setIdSupervisor(int idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public String toString() {
        return "RevisionEvaluacionEmpresa{" + "tipoEvaluacion=" + tipoEvaluacion + ", id=" + id + ", idRevision=" + idRevision + ", idIngeniero=" + idIngeniero + ", recomendacion=" + recomendacion + ", idSupervisor=" + idSupervisor + ", idEmpresa=" + idEmpresa + '}';
    }
            
}
