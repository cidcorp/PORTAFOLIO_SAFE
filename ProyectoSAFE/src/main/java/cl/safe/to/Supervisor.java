
package cl.safe.to;


public class Supervisor extends Persona{
    private int idSupervisor;

    public int getIdSupervisor() {
        return idSupervisor;
    }

    public void setIdSupervisor(int idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    @Override
    public String toString() {
        return "Supervisor{" + "idSupervisor=" + idSupervisor + '}';
    }
    
    
    
}
