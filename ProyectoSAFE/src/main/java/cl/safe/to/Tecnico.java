
package cl.safe.to;

public class Tecnico {
    
    private long id;
    private long rut;

    public Tecnico() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRut() {
        return rut;
    }

    public void setRut(long rut) {
        this.rut = rut;
    }

    @Override
    public String toString() {
        return "Tecnico{" + "id=" + id + ", rut=" + rut + '}';
    }
}
