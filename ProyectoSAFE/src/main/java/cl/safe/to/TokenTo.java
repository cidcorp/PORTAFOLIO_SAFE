package cl.safe.to;

import java.util.Date;

public class TokenTo {
    
    private boolean loginCorrecto;
    private String token;
    private Date fechaExpiracion; 

    public boolean isLoginCorrecto() {
        return loginCorrecto;
    }
    public void setLoginCorrecto(boolean loginCorrecto) {
        this.loginCorrecto = loginCorrecto;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }
    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }
    @Override
    public String toString() {
        return "TokenTo{" + "loginCorrecto=" + loginCorrecto + ", token=" + token + ", fechaExpiracion=" + fechaExpiracion + '}';
    }
     
}
