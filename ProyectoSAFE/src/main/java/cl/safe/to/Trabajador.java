package cl.safe.to;

public class Trabajador extends Persona{
    
    private int id;
    private String cargo;

    public Trabajador() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return "Trabajador{" + "id=" + id + ", cargo=" + cargo + '}';
    }

}
