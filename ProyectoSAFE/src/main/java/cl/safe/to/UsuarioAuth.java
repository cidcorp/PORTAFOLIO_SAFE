package cl.safe.to;

public class UsuarioAuth {
    private Usuario usuario;
    private Persona persona;
    private Empresa empresa;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    @Override
    public String toString() {
        return "UsuarioAuth{" + "usuario=" + usuario + ", persona=" + persona + ", empresa=" + empresa + '}';
    }
    
}
